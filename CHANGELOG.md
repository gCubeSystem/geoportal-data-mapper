
# Changelog

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v1.1.0]

- Added "Export as PDF" facility [#26026]

## [v1.0.1]

- Update maven-portal-bom version

## [v1.0.0]

- Fixed #25201
- First release
