package org.gcube.application.geoportaldatamapper;

import java.io.Serializable;
import java.util.List;

import org.gcube.application.geoportalcommon.shared.geoportal.config.GcubeProfileDV;
import org.gcube.portlets.widgets.mpformbuilder.shared.metadata.MetaDataProfileBean;

public class GcubeProfilesMetadataForUCD implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8953445402356442872L;
	private String profileID;
	private GcubeProfileDV gcubeProfile;
	private List<MetaDataProfileBean> listMetadataProfileBean;

	public GcubeProfilesMetadataForUCD() {

	}

	public GcubeProfilesMetadataForUCD(String profileID, GcubeProfileDV gcubeProfile,
			List<MetaDataProfileBean> listMetadataProfileBean) {

		this.profileID = profileID;
		this.gcubeProfile = gcubeProfile;
		this.listMetadataProfileBean = listMetadataProfileBean;
	}

	public String getProfileID() {
		return profileID;
	}

	public GcubeProfileDV getGcubeProfile() {
		return gcubeProfile;
	}

	public List<MetaDataProfileBean> getListMetadataProfileBean() {
		return listMetadataProfileBean;
	}

	public void setProfileID(String profileID) {
		this.profileID = profileID;
	}

	public void setGcubeProfile(GcubeProfileDV gcubeProfile) {
		this.gcubeProfile = gcubeProfile;
	}

	public void setListMetadataProfileBean(List<MetaDataProfileBean> listMetadataProfileBean) {
		this.listMetadataProfileBean = listMetadataProfileBean;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("GcubeProfilesMetadataForUCD [profileID=");
		builder.append(profileID);
		builder.append(", gcubeProfile=");
		builder.append(gcubeProfile);
		builder.append(", listMetadataProfileBean=");
		builder.append(listMetadataProfileBean);
		builder.append("]");
		return builder.toString();
	}

}
