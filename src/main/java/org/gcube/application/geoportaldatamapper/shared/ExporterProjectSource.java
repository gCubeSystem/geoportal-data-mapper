package org.gcube.application.geoportaldatamapper.shared;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ExporterProjectSource {

	// Required fields
	String profileID;
	String projectID;
	String scope;

	// Optional fields
	String profileTitle;
	String accountname; // if null the exporter will use PUBLIC ACCESS
	String gisLink;

}
