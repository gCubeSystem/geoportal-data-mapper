package org.gcube.application.geoportaldatamapper.shared;

import java.io.Serializable;
import java.net.URL;

public class FileReference implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5398263987693453776L;

	private URL storageVolatileURL;
	private String fileName;
	private String contentType;

	public FileReference() {

	}

	public URL getStorageVolatileURL() {
		return storageVolatileURL;
	}

	public String getFileName() {
		return fileName;
	}

	public void setStorageVolatileURL(URL storageVolatileURL) {
		this.storageVolatileURL = storageVolatileURL;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("FileReference [storageVolatileURL=");
		builder.append(storageVolatileURL);
		builder.append(", fileName=");
		builder.append(fileName);
		builder.append(", contentType=");
		builder.append(contentType);
		builder.append("]");
		return builder.toString();
	}

}
