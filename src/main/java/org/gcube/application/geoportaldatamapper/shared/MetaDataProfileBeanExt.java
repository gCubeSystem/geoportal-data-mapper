package org.gcube.application.geoportaldatamapper.shared;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.gcube.application.geoportalcommon.shared.geoportal.config.GcubeProfileDV;
import org.gcube.application.geoportalcommon.shared.geoportal.materialization.innerobject.FilesetDV;
import org.gcube.application.geoportalcommon.shared.geoportal.materialization.innerobject.PayloadDV;
import org.gcube.portlets.widgets.mpformbuilder.shared.metadata.CategoryWrapper;
import org.gcube.portlets.widgets.mpformbuilder.shared.metadata.MetaDataProfileBean;
import org.gcube.portlets.widgets.mpformbuilder.shared.metadata.MetadataFieldWrapper;

/**
 * The Class MetaDataProfileBeanExt.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Nov 23, 2022
 */
public class MetaDataProfileBeanExt extends MetaDataProfileBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2518128223147908835L;

	private String documentSectionAsJSON = null;

	private List<FilesetDV> listFileset = null;

	private GcubeProfileDV gcubeProfileDV;

	/**
	 * Instantiates a new meta data profile bean ext.
	 */
	public MetaDataProfileBeanExt() {

	}

	/**
	 * Instantiates a new meta data profile bean ext.
	 *
	 * @param type           the type
	 * @param title          the title
	 * @param metadataFields the metadata fields
	 * @param categories     the categories
	 */
	public MetaDataProfileBeanExt(String type, String title, List<MetadataFieldWrapper> metadataFields,
			List<CategoryWrapper> categories) {
		super(type, title, metadataFields, categories);
	}

	/**
	 * Gets the list fileset.
	 *
	 * @return the list fileset
	 */
	public List<FilesetDV> getListFileset() {
		if (listFileset == null)
			listFileset = new ArrayList<FilesetDV>();
		return listFileset;
	}

	/**
	 * Gets the document section as JSON.
	 *
	 * @return the document section as JSON
	 */
	public String getDocumentSectionAsJSON() {
		return documentSectionAsJSON;
	}

	/**
	 * Sets the document section as JSON.
	 *
	 * @param documentSectionAsJSON the new document section as JSON
	 */
	public void setDocumentSectionAsJSON(String documentSectionAsJSON) {
		this.documentSectionAsJSON = documentSectionAsJSON;
	}

	/**
	 * Sets the list fileset.
	 *
	 * @param listFileset the new list fileset
	 */
	public void setListFileset(List<FilesetDV> listFileset) {
		this.listFileset = listFileset;
	}

	/**
	 * Clone obj.
	 *
	 * @return the meta data profile bean ext
	 */
	public MetaDataProfileBeanExt cloneObj() {

		MetaDataProfileBeanExt clonedMDPBE = new MetaDataProfileBeanExt();
		clonedMDPBE.setTitle(this.getTitle());
		clonedMDPBE.setType(this.getType());
		ArrayList<FilesetDV> newListFileset = new ArrayList<FilesetDV>();
		for (FilesetDV filesetDV : this.getListFileset()) {
			FilesetDV newFileset = new FilesetDV();
			for (PayloadDV payloadDV : filesetDV.getListPayload()) {
				PayloadDV newPayloadDV = new PayloadDV();
				newPayloadDV.setLink(payloadDV.getLink());
				newPayloadDV.setMimetype(payloadDV.getMimetype());
				newPayloadDV.setName(payloadDV.getName());
				newPayloadDV.setStorageID(payloadDV.getStorageID());
				newFileset.addPayloadDV(newPayloadDV);
			}

			newListFileset.add(newFileset);
		}
		clonedMDPBE.setListFileset(newListFileset);

		ArrayList<MetadataFieldWrapper> newListMetadataFieldWrapper = new ArrayList<MetadataFieldWrapper>();

		for (MetadataFieldWrapper mfw : this.getMetadataFields()) {
			MetadataFieldWrapper newMfw = new MetadataFieldWrapper();
			newMfw.setAsGroup(mfw.getAsGroup());
			newMfw.setAsTag(mfw.getAsTag());
			List<String> list = mfw.getCurrentValues();
			if(list!=null) {
				newMfw.setCurrentValues(list.stream().toArray(String[]::new));
			}
			newMfw.setDefaultValue(mfw.getDefaultValue());
			newMfw.setFieldId(mfw.getFieldId());
			newMfw.setFieldName(mfw.getFieldName());
			newMfw.setFieldNameFromCategory(mfw.getFieldNameFromCategory());
			newMfw.setMandatory(mfw.getMandatory());
			newMfw.setMaxOccurs(mfw.getMaxOccurs());
			newMfw.setMultiSelection(mfw.isMultiSelection());
			newMfw.setNote(mfw.getNote());
			newMfw.setOwnerCategory(mfw.getOwnerCategory());
			newMfw.setType(mfw.getType());
			newMfw.setValidator(mfw.getValidator());
			newMfw.setVocabulary(mfw.getVocabulary());

			newListMetadataFieldWrapper.add(newMfw);

		}

		clonedMDPBE.setMetadataFields(newListMetadataFieldWrapper);

		return clonedMDPBE;
	}

	public void setGcubeProfile(GcubeProfileDV gcubeProfileDV) {
		this.gcubeProfileDV = gcubeProfileDV;

	}

	public GcubeProfileDV getGcubeProfileDV() {
		return gcubeProfileDV;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("MetaDataProfileBeanExt [documentSectionAsJSON=");
		builder.append(documentSectionAsJSON);
		builder.append(", listFileset=");
		builder.append(listFileset);
		builder.append(", gcubeProfileDV=");
		builder.append(gcubeProfileDV);
		builder.append("]");
		return builder.toString();
	}

}
