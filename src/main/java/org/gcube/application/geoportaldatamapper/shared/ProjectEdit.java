package org.gcube.application.geoportaldatamapper.shared;

import java.io.Serializable;
import java.util.List;

import org.gcube.application.geoportalcommon.shared.geoportal.project.ProjectDV;

public class ProjectEdit implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2885327516680245601L;

	private ProjectDV theProjectDV;

	private List<MetaDataProfileBeanExt> theProfileBeans;

	public ProjectEdit() {

	}

	public ProjectDV getTheProjectDV() {
		return theProjectDV;
	}

	public List<MetaDataProfileBeanExt> getTheProfileBeans() {
		return theProfileBeans;
	}

	public void setTheProjectDV(ProjectDV theProjectDV) {
		this.theProjectDV = theProjectDV;
	}

	public void setTheProfileBeans(List<MetaDataProfileBeanExt> theProfileBeans) {
		this.theProfileBeans = theProfileBeans;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ProjectEdit [theProjectDV=");
		builder.append(theProjectDV);
		builder.append(", theProfileBeans=");
		builder.append(theProfileBeans);
		builder.append("]");
		return builder.toString();
	}

}
