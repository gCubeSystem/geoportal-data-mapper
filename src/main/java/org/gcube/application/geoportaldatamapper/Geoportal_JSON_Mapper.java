package org.gcube.application.geoportaldatamapper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.function.Function;

import org.bson.Document;
import org.gcube.application.geoportal.client.utils.Serialization;
import org.gcube.application.geoportal.common.model.document.access.Access;
import org.gcube.application.geoportal.common.model.document.access.AccessPolicy;
import org.gcube.application.geoportalcommon.ConvertToDataServiceModel;
import org.gcube.application.geoportalcommon.ConvertToDataValueObjectModel;
import org.gcube.application.geoportalcommon.geoportal.access.GeportalCheckAccessPolicy;
import org.gcube.application.geoportalcommon.geoportal.serdes.Payload;
import org.gcube.application.geoportalcommon.shared.geoportal.config.FilePathDV;
import org.gcube.application.geoportalcommon.shared.geoportal.config.GcubeProfileDV;
import org.gcube.application.geoportalcommon.shared.geoportal.materialization.GCubeSDIViewerLayerDV;
import org.gcube.application.geoportalcommon.shared.geoportal.materialization.innerobject.BBOXDV;
import org.gcube.application.geoportalcommon.shared.geoportal.materialization.innerobject.FilesetDV;
import org.gcube.application.geoportalcommon.shared.geoportal.materialization.innerobject.PayloadDV;
import org.gcube.application.geoportalcommon.shared.geoportal.project.ProjectDV;
import org.gcube.application.geoportalcommon.shared.geoportal.ucd.GEOPORTAL_CONFIGURATION_TYPE;
import org.gcube.application.geoportalcommon.shared.geoportal.view.ProjectView;
import org.gcube.application.geoportalcommon.shared.geoportal.view.SectionView;
import org.gcube.application.geoportalcommon.shared.geoportal.view.SubDocumentView;
import org.gcube.application.geoportaldatamapper.shared.MetaDataProfileBeanExt;
import org.gcube.application.geoportaldatamapper.shared.ProjectEdit;
import org.gcube.portlets.widgets.mpformbuilder.shared.metadata.MetaDataProfileBean;
import org.gcube.portlets.widgets.mpformbuilder.shared.metadata.MetadataFieldWrapper;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.spi.json.JsonOrgJsonProvider;

/**
 * The Class Geoportal_JSON_Mapper.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Nov 30, 2022
 */
public class Geoportal_JSON_Mapper {

	public static final String _PAYLOADS = "_payloads";

	public static final String _MATERIALIZATIONS = "_materializations";

	public static final String _TYPE_GCUBE_SDI_LAYER = "gcube-sdi-layer";

	private static final Logger LOG = LoggerFactory.getLogger(Geoportal_JSON_Mapper.class);

	public static final String FILESET = "fileset";

	public static final String _OGC_LINKS = "_ogcLinks";

	public static final String _BBOX = "_bbox";

	public static final String _TYPE = "_type";

	public static final String JSON_$_POINTER = "$";

	public static final String _THEDOCUMENT = "_theDocument";

	/**
	 * Load project edit.
	 *
	 * @param theProjectDV the the project DV
	 * @param scope        the scope
	 * @param username     the username
	 * @return the project edit
	 * @throws Exception the exception
	 */
	public static ProjectEdit loadProjectEdit(ProjectDV theProjectDV, String scope, String username) throws Exception {
		LOG.debug("loadProjectEdit called");

		String theWholeProjectAsJSON = theProjectDV.getTheDocument().getDocumentAsJSON();

		LOG.debug("theProjectDV as JSON: " + theWholeProjectAsJSON);
		LOG.trace("theProjectDV as MAP: " + theProjectDV.getTheDocument().getDocumentAsMap());

		ProjectEdit projectEdit = new ProjectEdit();
		projectEdit.setTheProjectDV(theProjectDV);

		LinkedHashMap<String, List<GcubeProfilesMetadataForUCD>> linkedMap_UCDId_gCubeProfiles = GcubeProfilesPerUCDIdCache
				.get(scope);

		// NO UCD defined, applying default
		if (linkedMap_UCDId_gCubeProfiles.size() == 0) {
			LOG.warn("No " + GEOPORTAL_CONFIGURATION_TYPE.gcube_profiles + " found in the UCD");
			LOG.info("Applying default business logic to display the project");
			SectionView sectionView = new SectionView();
			sectionView.setSectionTitle("Document");
			Document sectionDoc = Document.parse(theProjectDV.getTheDocument().getDocumentAsJSON());

			// Creating one Project with one SectionView and SubDocumentView
			String wholeSectionDoc = sectionDoc.toJson();

			List<FilesetDV> listFiles = new ArrayList<FilesetDV>();

			// Reading Fileset _payloads
			String filesetJSONPath = String.format("%s.%s", JSON_$_POINTER, FILESET);
			List<Payload> listPayloads = readPayloadsForFileset(filesetJSONPath, wholeSectionDoc);
			FilesetDV filesetDV = new FilesetDV();
			filesetDV.setGcubeProfileFieldName(FILESET);
			for (Payload payload : listPayloads) {
				PayloadDV payloadDV = ConvertToDataValueObjectModel.toPayloadDV(payload);
				filesetDV.addPayloadDV(payloadDV);
				listFiles.add(filesetDV);
			}
		}

		List<GcubeProfilesMetadataForUCD> listProfilesBean = linkedMap_UCDId_gCubeProfiles
				.get(theProjectDV.getProfileID());

		com.jayway.jsonpath.Configuration configuration = com.jayway.jsonpath.Configuration.builder()
				.jsonProvider(new JsonOrgJsonProvider()).build();

		List<MetaDataProfileBeanExt> listOfProfilesBeanExt = new ArrayList<MetaDataProfileBeanExt>();

		// Reading the Project according to list of Profile defined in the UCD
		for (GcubeProfilesMetadataForUCD gcubeProfileMetaForUCD : listProfilesBean) {

			GcubeProfileDV gcubeProfileDV = gcubeProfileMetaForUCD.getGcubeProfile();
			LOG.info("\n\n##### Creating the section: " + gcubeProfileDV.getSectionTitle());
			LOG.debug("\n\nThe profile is: " + gcubeProfileDV);
			// Building JSON/section full PATH and section name
			String sectionJSONPath = "";
			String parentPathFromProfile = gcubeProfileDV.getParentName() == null ? "" : gcubeProfileDV.getParentName();
			String theSectionName = gcubeProfileDV.getSectionName();

			if (theSectionName.compareTo(JSON_$_POINTER) == 0 || theSectionName.compareTo(JSON_$_POINTER + ".") == 0) {
				sectionJSONPath = JSON_$_POINTER;
				theSectionName = "";
			} else {
				sectionJSONPath = String.format("%s%s",
						parentPathFromProfile.endsWith(".") ? parentPathFromProfile : parentPathFromProfile + ".",
						theSectionName);
			}

			LOG.debug("The sectionJSONPath is: " + sectionJSONPath);

			JsonPath theSectionJsonPath = null;
			Object data = null;
			try {
				theSectionJsonPath = JsonPath.compile(sectionJSONPath);
				data = theSectionJsonPath.read(theWholeProjectAsJSON, configuration);
			} catch (Exception e) {
				LOG.warn("Error on searching the section " + sectionJSONPath + " in the JSON Project: "
						+ theWholeProjectAsJSON);
				continue;
			}

			LOG.debug("Data is instace of: " + data.getClass());
			LOG.debug("data to string: " + data.toString());

			// Splitting the General Document in bson.Document according to list of
			// GcubeProfiles
			List<Document> listBSONDocument = new ArrayList<Document>();
			if (data instanceof org.json.JSONObject) {
				String jsonString = data.toString();
				LOG.debug("the JSON to string: " + jsonString);
				Document sectionDoc = Document.parse(jsonString);
				listBSONDocument.add(sectionDoc);

			} else if (data instanceof org.json.JSONArray) {
				org.json.JSONArray dataArray = (org.json.JSONArray) data;
				for (int i = 0; i < dataArray.length(); i++) {
					String jsonString = dataArray.get(i).toString();
					LOG.debug("the array " + i + " JSON to string: " + jsonString);
					Document sectionDoc = Document.parse(jsonString);
					listBSONDocument.add(sectionDoc);
				}
			}

			LOG.debug("Result for " + gcubeProfileDV.getSectionName() + " is: " + listBSONDocument);
			List<MetaDataProfileBean> theProfileBeans = gcubeProfileMetaForUCD.getListMetadataProfileBean();
			MetaDataProfileBean theProfileBean = theProfileBeans.get(0);

			// For each bson.Document filling the MetaDataProfileBean and its file
			for (int i = 0; i < listBSONDocument.size(); i++) {

				LOG.trace("DOCUMENT number " + i + " of the section: " + theProfileBean.getTitle());
				MetaDataProfileBeanExt theProfileBeanExt = new MetaDataProfileBeanExt();
				theProfileBeanExt.setCategories(theProfileBean.getCategories());
				theProfileBeanExt.setTitle(theProfileBean.getTitle());
				theProfileBeanExt.setType(theProfileBean.getType());
				theProfileBeanExt.setGcubeProfile(gcubeProfileDV);
				Document fromSectionDoc = listBSONDocument.get(i);
				LOG.debug("\n\nNew section DOC for index " + i + " is: "
						+ new JSONObject(fromSectionDoc.toJson()).toString(2));
				// Creating the corresponding MetaDataProfileBeanExt for each section

				// Reading policy and license statically
				// eg. "_access":{"_policy":"OPEN","_license":"CC0-1.0"}}
				Access access = null;
				try {

					List<FilePathDV> fileSetPaths = gcubeProfileDV.getFilePaths();
					if (fileSetPaths != null && fileSetPaths.size() > 0) {
						FilePathDV firstOne = fileSetPaths.get(0);
						access = getAccessPolicyObject(fromSectionDoc.toJson(),
								JSON_$_POINTER + "." + firstOne.getFieldName());

					}
				} catch (Exception e) {
					LOG.warn("No " + AccessPolicy.class.getSimpleName() + " found in the section "
							+ fromSectionDoc.toJson());
					LOG.debug("No AccessPolicy.class.getSimpleName(): ", e);
				}

				List<MetadataFieldWrapper> cloneListOfMFW = cloneList(theProfileBean.getMetadataFields());

				List<MetadataFieldWrapper> duplicatedList = new ArrayList<MetadataFieldWrapper>();

				for (MetadataFieldWrapper metadataField : cloneListOfMFW) {

					String theFieldName = metadataField.getFieldId() != null ? metadataField.getFieldId()
							: metadataField.getFieldName();
					LOG.debug("reading theFieldName: " + theFieldName);

					if (access != null) {
						if (theFieldName.equalsIgnoreCase("policy")) {
							metadataField.setCurrentValues(access.getPolicy().name());
							duplicatedList.add(metadataField);
							continue;
						} else if (theFieldName.equalsIgnoreCase("licenseID")) {
							metadataField.setCurrentValues(access.getLicense());
							duplicatedList.add(metadataField);
							continue;
						}
					}

					Object theOBJFieldValue = fromSectionDoc.get(theFieldName);

					if (theOBJFieldValue != null) {
						// Converting multiple values stored as array (e.g. [a,b,c]) in multiple
						// MetadataFieldWrapper
						// repeatable fields
						LOG.trace("converting value " + theOBJFieldValue);
						LOG.trace("converting class " + theOBJFieldValue.getClass());
						try {
							LOG.debug("It is an Array");
							List dataArray = (List) theOBJFieldValue;
							//printList(dataArray);
							int dataSize = dataArray.size();
							for (int j = 0; j < dataSize; j++) {
								List<MetadataFieldWrapper> cloned = cloneList(Arrays.asList(metadataField));
								MetadataFieldWrapper mfw = cloned.get(0);
								
								//Duplicating MetadataFieldWrapper for data list with isMultiSelection==false
								if(!mfw.isMultiSelection()) {
									mfw.setCurrentValues(dataArray.get(j)+"");
									// In case of array, from the first to second-last one, repeated field is set to 'false'
									// These properties are managed properly with the
									// last one field
									if (j < dataSize-1) {
										mfw.setMandatory(false);
										mfw.setMaxOccurs(1);
									}
									duplicatedList.add(mfw);
								}else {
									//Setting dataArray as list of current values when isMultiSelection is true
									String[] toArray = (String[]) dataArray.toArray(new String[0]);
									mfw.setCurrentValues(toArray);
									duplicatedList.add(mfw);
									//Exit from for
									break;
								}
							}
							//Does not remove this
							continue;
						} catch (Exception e) {
							LOG.debug("It is not an Array");
							String theValue = theOBJFieldValue + "";
							metadataField.setCurrentValues(theValue);
						}
					}

					duplicatedList.add(metadataField);

				}

				theProfileBeanExt.setMetadataFields(new ArrayList<MetadataFieldWrapper>(duplicatedList));
				theProfileBeanExt.setDocumentSectionAsJSON(fromSectionDoc.toJson());
				theProfileBeanExt.setGcubeProfile(gcubeProfileDV);
				LOG.debug("Metadata fields are: " + theProfileBeanExt.getMetadataFields());

				// Reading filePaths
				List<FilePathDV> filePaths = gcubeProfileDV.getFilePaths();

				// READING fileset* field ACCORDING TO filePaths OF THE 'gcubeProfiles' CONFIG
				if (filePaths != null) {
					String fromSectionDocJSON = fromSectionDoc.toJson();
					List<FilesetDV> listFiles = new ArrayList<FilesetDV>();
//					List<GCubeSDIViewerLayerDV> listLayers = new ArrayList<GCubeSDIViewerLayerDV>();
					for (FilePathDV filePath : filePaths) {

						// Reading Fileset _payloads
						String filesetJSONPath = String.format("%s.%s", JSON_$_POINTER, filePath.getFieldName());
						List<Payload> listPayloads = readPayloadsForFileset(filesetJSONPath, fromSectionDocJSON);
						FilesetDV filesetDV = new FilesetDV();
						filesetDV.setGcubeProfileFieldName(filePath.getGcubeProfileFieldName());
						filesetDV.setFilesetFieldName(filePath.getFieldName());
						for (Payload payload : listPayloads) {
							PayloadDV payloadDV = ConvertToDataValueObjectModel.toPayloadDV(payload);
							filesetDV.addPayloadDV(payloadDV);
						}

						listFiles.add(filesetDV);
					}

					theProfileBeanExt.setListFileset(listFiles);
				}

				LOG.trace("\nputting theProfileBeanExt: " + theProfileBeanExt);
				listOfProfilesBeanExt.add(theProfileBeanExt);
			}

		}

		projectEdit.setTheProfileBeans(listOfProfilesBeanExt);

		LOG.info("returning ProjectEdit with " + projectEdit.getTheProfileBeans().size() + " profile beans");

		return projectEdit;
	}
	
	public static void printArrayValue(JSONArray dataArray) {
		if(dataArray==null)
			System.out.println("dataArray is null");
		
		for (int i =0; i<dataArray.length(); i++) {
			try {
				System.out.println("dataArray index "+i+" value "+dataArray.get(i));
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public static void printList(List dataArray) {
		if(dataArray==null)
			System.out.println("ArrayList is null");
		
		for (int i =0; i<dataArray.size(); i++) {
			System.out.println("ArrayList index "+i+" value "+dataArray.get(i));
			
		}
	}

	/**
	 * Clone list.
	 *
	 * @param list the list
	 * @return the list
	 */
	public static List<MetadataFieldWrapper> cloneList(List<MetadataFieldWrapper> list) {
		List<MetadataFieldWrapper> listCloned = new ArrayList<MetadataFieldWrapper>(list.size());

		Function<MetadataFieldWrapper, MetadataFieldWrapper> cloneWrapper = (mfw) -> {

			MetadataFieldWrapper newMfw = new MetadataFieldWrapper();
			newMfw.setAsGroup(mfw.getAsGroup());
			newMfw.setAsTag(mfw.getAsTag());
			List<String> listValues = mfw.getCurrentValues();
			if(listValues!=null) {
				newMfw.setCurrentValues(listValues.stream().toArray(String[]::new));
			}
			newMfw.setDefaultValue(mfw.getDefaultValue());
			newMfw.setFieldId(mfw.getFieldId());
			newMfw.setFieldName(mfw.getFieldName());
			newMfw.setFieldNameFromCategory(mfw.getFieldNameFromCategory());
			newMfw.setMandatory(mfw.getMandatory());
			newMfw.setMaxOccurs(mfw.getMaxOccurs());
			newMfw.setMultiSelection(mfw.isMultiSelection());
			newMfw.setNote(mfw.getNote());
			newMfw.setOwnerCategory(mfw.getOwnerCategory());
			newMfw.setType(mfw.getType());
			newMfw.setValidator(mfw.getValidator());
			newMfw.setVocabulary(mfw.getVocabulary());

			return newMfw;

		};

		for (MetadataFieldWrapper item : list) {
			MetadataFieldWrapper cloned = cloneWrapper.apply(item);
			listCloned.add(cloned);
		}
		return listCloned;
	}

	/**
	 * Load project view.
	 *
	 * @param theProjectDV the the project DV
	 * @param scope        the scope
	 * @param username     the username
	 * @return the project view
	 * @throws Exception the exception
	 */
	public static ProjectView loadProjectView(ProjectDV theProjectDV, String scope, String username) throws Exception {

		String theWholeProjectAsJSON = theProjectDV.getTheDocument().getDocumentAsJSON();

		LOG.debug("theProjectDV as JSON: " + theWholeProjectAsJSON);
		LOG.debug("theProjectDV as MAP: " + theProjectDV.getTheDocument().getDocumentAsMap());

		ProjectView projectView = new ProjectView();
		projectView.setTheProjectDV(theProjectDV);

		LinkedHashMap<String, List<GcubeProfilesMetadataForUCD>> linkedMap_UCDId_gCubeProfiles = GcubeProfilesPerUCDIdCache
				.get(scope);

		// NO UCD defined, applying default
		//Never checked. It could be buggy
		if (linkedMap_UCDId_gCubeProfiles.size() == 0) {
			LOG.warn("No " + GEOPORTAL_CONFIGURATION_TYPE.gcube_profiles + " found in the UCD");
			LOG.info("Applying default business logic to display the project");
			SectionView sectionView = new SectionView();
			sectionView.setSectionTitle("Document");
			SubDocumentView subDocumentView = new SubDocumentView();

			Document sectionDoc = Document.parse(theProjectDV.getTheDocument().getDocumentAsJSON());
			boolean isAccessibleSection = isAccessibleSectionAccordingToPolicy(sectionDoc, JSON_$_POINTER, username);

			// If is accessible
			if (isAccessibleSection) {

				// Creating one Project with one SectionView and SubDocumentView
				String wholeSectionDoc = sectionDoc.toJson();
				subDocumentView.setMetadataAsJSON(wholeSectionDoc);

				List<FilesetDV> listFiles = new ArrayList<FilesetDV>();
				List<FilesetDV> listImages = new ArrayList<FilesetDV>();
				List<GCubeSDIViewerLayerDV> listLayers = new ArrayList<GCubeSDIViewerLayerDV>();

				// Reading Fileset _payloads
				String filesetJSONPath = String.format("%s.%s", JSON_$_POINTER, FILESET);
				List<Payload> listPayloads = readPayloadsForFileset(filesetJSONPath, wholeSectionDoc);
				FilesetDV filesetDV = new FilesetDV();
				filesetDV.setGcubeProfileFieldName(FILESET);
				for (Payload payload : listPayloads) {
					PayloadDV payloadDV = ConvertToDataValueObjectModel.toPayloadDV(payload);
					filesetDV.addPayloadDV(payloadDV);
					boolean isImage = ImageDetector.isImage(payload.getMimetype());

					if (isImage) {
						listImages.add(filesetDV);
					} else {
						listFiles.add(filesetDV);
					}
				}

				subDocumentView.setListImages(listImages);
				subDocumentView.setListFiles(listFiles);

				// Reading Fileset _materializations / layers
				listLayers = readGcubeSDILayersForFileset(filesetJSONPath, wholeSectionDoc);

				subDocumentView.setListLayers(listLayers);
				sectionView.addSubDocument(subDocumentView);
				projectView.addSectionView(sectionView);

			}

		}

		List<GcubeProfilesMetadataForUCD> listProfilesBean = linkedMap_UCDId_gCubeProfiles
				.get(theProjectDV.getProfileID());

		com.jayway.jsonpath.Configuration configuration = com.jayway.jsonpath.Configuration.builder()
				.jsonProvider(new JsonOrgJsonProvider()).build();

		// Reading the Project according to list of Profile defined in the UCD
		for (GcubeProfilesMetadataForUCD gcubeProfileMetaForUCD : listProfilesBean) {

			GcubeProfileDV gcubeProfileDV = gcubeProfileMetaForUCD.getGcubeProfile();
			SectionView sectionView = new SectionView();
			sectionView.setSectionTitle(gcubeProfileDV.getSectionTitle());
			LOG.debug("\n\nThe profile is: " + gcubeProfileDV);
			// Building JSON/section full PATH and section name
			String sectionJSONPath = "";
			String parentPathFromProfile = gcubeProfileDV.getParentName() == null ? "" : gcubeProfileDV.getParentName();
			String theSectionName = gcubeProfileDV.getSectionName();

			if (theSectionName.compareTo(JSON_$_POINTER) == 0 || theSectionName.compareTo(JSON_$_POINTER + ".") == 0) {
				sectionJSONPath = JSON_$_POINTER;
				theSectionName = "";
			} else {
				sectionJSONPath = String.format("%s%s",
						parentPathFromProfile.endsWith(".") ? parentPathFromProfile : parentPathFromProfile + ".",
						theSectionName);
			}

			LOG.debug("The sectionJSONPath is: " + sectionJSONPath);

			JsonPath theSectionJsonPath = null;
			Object data = null;
			try {
				theSectionJsonPath = JsonPath.compile(sectionJSONPath);
				data = theSectionJsonPath.read(theWholeProjectAsJSON, configuration);
			} catch (Exception e) {
				LOG.warn("Error on searching the section " + sectionJSONPath + " in the JSON Project: "
						+ theWholeProjectAsJSON);
				continue;
			}

			LOG.debug("Data is instace of: " + data.getClass());
			LOG.debug("data to string: " + data.toString());
			
			List<FilePathDV> fileSetPaths = gcubeProfileDV.getFilePaths();
			LOG.debug("gcubeProfileDV "+gcubeProfileDV.getGcubeName() + " fileSetPaths: "+fileSetPaths);
			String filesetPath = JSON_$_POINTER;
			if (fileSetPaths != null && fileSetPaths.size() > 0) {
				//Reading the first fieldName that defines the name of the "fileset" field
				FilePathDV firstOne = fileSetPaths.get(0);
				filesetPath +="."+firstOne.getFieldName();

			}
			LOG.debug("filesetPath is: "+filesetPath);

			// Splitting the General Document in bson.Document according to list of
			// GcubeProfiles
			List<Document> listBSONDocument = new ArrayList<Document>();
			if (data instanceof org.json.JSONObject) {
				String jsonString = data.toString();
				LOG.debug("the JSON to string: " + jsonString);
				Document sectionDoc = Document.parse(jsonString);
				boolean isAccessibleSection = isAccessibleSectionAccordingToPolicy(sectionDoc, filesetPath,
						username);
				if (isAccessibleSection) {
					listBSONDocument.add(sectionDoc);
				}

			} else if (data instanceof org.json.JSONArray) {
				org.json.JSONArray dataArray = (org.json.JSONArray) data;
				for (int i = 0; i < dataArray.length(); i++) {
					String jsonString = dataArray.get(i).toString();
					LOG.debug("the array " + i + " JSON to string: " + jsonString);
					Document sectionDoc = Document.parse(jsonString);
					boolean isAccessibleSection = isAccessibleSectionAccordingToPolicy(sectionDoc, filesetPath,
							username);
					if (isAccessibleSection) {
						listBSONDocument.add(sectionDoc);
					}

				}
			}

			LOG.debug("Result for " + gcubeProfileDV.getSectionName() + " is: " + listBSONDocument);
			List<MetaDataProfileBean> theProfileBeans = gcubeProfileMetaForUCD.getListMetadataProfileBean();
			MetaDataProfileBean theProfileBean = theProfileBeans.get(0);

			// For each bson.Document creating the SubDocumentView
			for (int i = 0; i < listBSONDocument.size(); i++) {
				Document fromSectionDoc = listBSONDocument.get(i);
				SubDocumentView subDocumentView = new SubDocumentView();
				Document toSectionDoc = new Document();
				// Filling the SubDocumentView metadata with the metadataField.getFieldName()
				// read from the Profile
				for (MetadataFieldWrapper metadataField : theProfileBean.getMetadataFields()) {

					String theFieldName = metadataField.getFieldId() != null ? metadataField.getFieldId()
							: metadataField.getFieldName();
					LOG.debug("reading theFieldName: " + theFieldName);
					Object theOBJFieldValue = fromSectionDoc.get(theFieldName);

					// NB: Using ALWAYS THE metadataField.getFieldName() as LABEL
					toSectionDoc = sanitizeDocumentValue(toSectionDoc, metadataField.getFieldName(), theOBJFieldValue);

				}
				String subToJSON = toSectionDoc.toJson();
				LOG.debug("theSubSetionDoc is: " + subToJSON);
				subDocumentView.setMetadataAsJSON(toSectionDoc.toJson());

				// Reading filePaths
				List<FilePathDV> filePaths = gcubeProfileDV.getFilePaths();

				// READING fileset* field ACCORDING TO filePaths OF THE 'gcubeProfiles' CONFIG
				if (filePaths != null) {
					String fromSectionDocJSON = fromSectionDoc.toJson();
					List<FilesetDV> listFiles = new ArrayList<FilesetDV>();
					List<FilesetDV> listImages = new ArrayList<FilesetDV>();
					List<GCubeSDIViewerLayerDV> listLayers = new ArrayList<GCubeSDIViewerLayerDV>();
					for (FilePathDV filePath : filePaths) {

						// Reading Fileset _payloads
						String filesetJSONPath = String.format("%s.%s", JSON_$_POINTER, filePath.getFieldName());
						List<Payload> listPayloads = readPayloadsForFileset(filesetJSONPath, fromSectionDocJSON);
						if (LOG.isDebugEnabled()) {
							for (Payload payload : listPayloads) {
								LOG.debug("read payload: " + payload);
							}
						}

						FilesetDV filesetImages = new FilesetDV();
						FilesetDV filesetFiles = new FilesetDV();

						for (Payload payload : listPayloads) {
							PayloadDV payloadDV = ConvertToDataValueObjectModel.toPayloadDV(payload);
							// filesetDV.addPayloadDV(payloadDV);
							boolean isImage = ImageDetector.isImage(payload.getMimetype());

							if (isImage) {
								filesetImages.setGcubeProfileFieldName(filePath.getGcubeProfileFieldName());
								filesetImages.addPayloadDV(payloadDV);
								filesetFiles.setFilesetFieldName(filePath.getFieldName());
							} else {
								filesetFiles.addPayloadDV(payloadDV);
								filesetFiles.setGcubeProfileFieldName(filePath.getGcubeProfileFieldName());
								filesetFiles.setFilesetFieldName(filePath.getFieldName());
							}
						}

						// Setting only if one element exists
						if (filesetImages.getListPayload() != null && filesetImages.getListPayload().size() > 0) {
							listImages.add(filesetImages);
						}
						if (filesetFiles.getListPayload() != null && filesetFiles.getListPayload().size() > 0) {
							listFiles.add(filesetFiles);
						}

						// Reading Fileset _materializations
						listLayers = readGcubeSDILayersForFileset(filesetJSONPath, fromSectionDocJSON);

					}
					subDocumentView.setListFiles(listFiles);
					subDocumentView.setListImages(listImages);
					subDocumentView.setListLayers(listLayers);
				}

				sectionView.addSubDocument(subDocumentView);

			}

			projectView.addSectionView(sectionView);
		}

		LOG.debug("returning the projectView: " + projectView);
		return projectView;
	}

	/**
	 * Read payloads for fileset.
	 *
	 * @param filesetJSONPath     the fileset JSON path
	 * @param sectionJSONDocument the section JSON document
	 * @return the list
	 */
	public static List<Payload> readPayloadsForFileset(String filesetJSONPath, String sectionJSONDocument) {
		LOG.debug("readPayloadsForFileset called");

		List<Payload> listPayloads = new ArrayList<Payload>();
		String _payloadsJSONPath = String.format("%s.%s", filesetJSONPath, _PAYLOADS);
		try {
			com.jayway.jsonpath.Configuration configuration = com.jayway.jsonpath.Configuration.builder()
					.jsonProvider(new JsonOrgJsonProvider()).build();

			LOG.debug("Reading sectionPath at {} into section document {}", _payloadsJSONPath, sectionJSONDocument);
			JsonPath theSectionPolycJsonPath = JsonPath.compile(_payloadsJSONPath);
			Object _payloads = theSectionPolycJsonPath.read(sectionJSONDocument, configuration).toString();

			if (_payloads instanceof String) {
				String toStringPayloads = (String) _payloads;
				LOG.trace("The _payloads is a String {}", toStringPayloads);
				JSONArray jsonArray = new JSONArray(toStringPayloads);
				for (int i = 0; i < jsonArray.length(); i++) {
					Payload payload = Serialization.read(jsonArray.getJSONObject(i).toString(), Payload.class);
					listPayloads.add(payload);
				}
			}

			LOG.info("returning list of payloads {}", listPayloads);
		} catch (Exception e) {
			LOG.warn("Error on reading the JSON Path " + _payloadsJSONPath + " in the doc " + sectionJSONDocument, e);
		}

		return listPayloads;

	}

	/**
	 * Read images for fileset.
	 *
	 * @param parentJSONPath       the parent JSON path
	 * @param sectionJSONDocument  the section JSON document
	 * @param limitToFirstOneFound the limit to first one found
	 * @return the list
	 */
	public static List<PayloadDV> readImagesForFileset(String parentJSONPath, String sectionJSONDocument,
			boolean limitToFirstOneFound) {
		LOG.debug("readImagesForFileset called");

		List<PayloadDV> listImages = new ArrayList<PayloadDV>();
		com.jayway.jsonpath.Configuration config = com.jayway.jsonpath.Configuration.builder()
				.jsonProvider(new JsonOrgJsonProvider()).build();
		// Reading Fileset _payloads
		String filesetJSONPath = String.format("%s.%s", parentJSONPath, _PAYLOADS);
		LOG.debug("Reading sectionPath {} into section document {}", filesetJSONPath, sectionJSONDocument);
		JsonPath theSectionPolycJsonPath = JsonPath.compile(filesetJSONPath);
		Object _filesets = theSectionPolycJsonPath.read(sectionJSONDocument, config);

		List<Payload> payloads = new ArrayList<Payload>();
		List<Payload> listPayloads = null;
		try {
			listPayloads = recursiveFetchingPayloads(config, _filesets, payloads);
		} catch (Exception e) {
			LOG.warn("Error occurred on fetching the payloads: ", e);
		}
		if (listPayloads != null) {
			for (Payload payload : listPayloads) {
				boolean isImage = ImageDetector.isImage(payload.getMimetype());
				if (isImage) {
					PayloadDV payloadDV = ConvertToDataValueObjectModel.toPayloadDV(payload);
					listImages.add(payloadDV);
					if (limitToFirstOneFound && listImages.size() == 1)
						return listImages;
				}
			}
		}

		LOG.debug("readImagesForFileset returning listOfImages: " + listImages);
		return listImages;

	}

	/**
	 * Read gcube SDI layers for fileset.
	 *
	 * @param materializationParentJSONPath the materialization parent JSON path
	 * @param sectionJSONDocument           the section JSON document
	 * @return the list
	 */
	public static List<GCubeSDIViewerLayerDV> readGcubeSDILayersForFileset(String materializationParentJSONPath,
			String sectionJSONDocument) {
		LOG.debug("readGcubeSDILayersForFileset called");

		List<GCubeSDIViewerLayerDV> listSDILayers = new ArrayList<GCubeSDIViewerLayerDV>();
		// _materializations AT THE MOMENT ARE ONLY OF TYPE gcube-sdi-layer. I'm adding
		// the filtering "_type": "gcube-sdi-layer" to be sure of reading SDI layer
		String _materializationsJSONPath = String.format("%s.%s[?(@._type=='%s')]", materializationParentJSONPath,
				_MATERIALIZATIONS, _TYPE_GCUBE_SDI_LAYER);

		try {
			com.jayway.jsonpath.Configuration configurationJSONSmart = com.jayway.jsonpath.Configuration.builder()
					.jsonProvider(new JsonOrgJsonProvider()).build();

			LOG.debug("GcubeSDILayers Reading sectionPath {} into section document {}", _materializationsJSONPath,
					sectionJSONDocument);
			JsonPath theSectionMaterializationJsonPath = JsonPath.compile(_materializationsJSONPath);
			Object _materializations = theSectionMaterializationJsonPath.read(sectionJSONDocument,
					configurationJSONSmart);

			LOG.trace("_materializations.class(): " + _materializations.getClass());

			listSDILayers = recursiveFetchingLayers(configurationJSONSmart, _materializations, listSDILayers);

		} catch (Exception e) {
			LOG.info("Error on reading the JSON Path " + _materializationsJSONPath + " in the doc "
					+ sectionJSONDocument, e);
		}
		LOG.info("returning list of layers {}", listSDILayers);
		return listSDILayers;

	}

	/**
	 * Recursive fetching layers.
	 *
	 * @param config        the config
	 * @param objectJSON    the object JSON
	 * @param listSDILayers the list SDI layers
	 * @return the list
	 * @throws JSONException the JSON exception
	 */
	private static List<GCubeSDIViewerLayerDV> recursiveFetchingLayers(com.jayway.jsonpath.Configuration config,
			Object objectJSON, List<GCubeSDIViewerLayerDV> listSDILayers) throws JSONException {

		if (objectJSON == null)
			return listSDILayers;

		if (objectJSON instanceof JSONArray) {
			JSONArray theJsonArray = (JSONArray) objectJSON;
			LOG.trace("jsonArray: " + theJsonArray.toString(3));

			for (int i = 0; i < theJsonArray.length(); i++) {
				recursiveFetchingLayers(config, theJsonArray.get(i), listSDILayers);
			}
		} else if (objectJSON instanceof JSONObject) {
			JSONObject theJsonObject = (JSONObject) objectJSON;
			LOG.trace("theJSONObject: " + theJsonObject.toString(3));
			GCubeSDIViewerLayerDV gsdiLayer = convertLayer(config, theJsonObject);
			listSDILayers.add(gsdiLayer);
		}

		return listSDILayers;
	}

	/**
	 * Recursive fetching payloads.
	 *
	 * @param config     the config
	 * @param objectJSON the object JSON
	 * @param payloads   the payloads
	 * @return the list
	 * @throws JSONException the JSON exception
	 */
	public static List<Payload> recursiveFetchingPayloads(com.jayway.jsonpath.Configuration config, Object objectJSON,
			List<Payload> payloads) throws JSONException {
		LOG.debug("recursiveFetchingPayloads called");

		if (objectJSON == null)
			return payloads;

		if (objectJSON instanceof JSONArray) {
			JSONArray theJsonArray = (JSONArray) objectJSON;
			LOG.trace("jsonArray: " + theJsonArray.toString(3));

			for (int i = 0; i < theJsonArray.length(); i++) {
				payloads = recursiveFetchingPayloads(config, theJsonArray.get(i), payloads);
			}
		} else if (objectJSON instanceof JSONObject) {
			JSONObject toStringPayloads = (JSONObject) objectJSON;
			LOG.trace("The _payloads is a String {}", toStringPayloads.toString(3));
			Payload payload;
			try {
				payload = Serialization.read(toStringPayloads.toString(), Payload.class);
				payloads.add(payload);
			} catch (Exception e) {
				LOG.warn("Error on converting the JSON Boject " + toStringPayloads + "as "
						+ Payload.class.getSimpleName() + e.getMessage());
			}

		}

		return payloads;

	}

	/**
	 * Convert layer.
	 *
	 * @param config       the config
	 * @param thJsonObject the th json object
	 * @return the g cube SDI viewer layer DV
	 */
	// TODO THIS PART SHOULD BE REVISITED/OPTIMIZED
	private static GCubeSDIViewerLayerDV convertLayer(com.jayway.jsonpath.Configuration config,
			JSONObject thJsonObject) {
		LOG.debug("converLayer called for " + thJsonObject);

		GCubeSDIViewerLayerDV gsdiLayer = new GCubeSDIViewerLayerDV();
		try {
			String theType = thJsonObject.getString(_TYPE);
			gsdiLayer.setType(theType);
			LOG.debug(_TYPE + " is: " + theType);
		} catch (Exception e) {
			LOG.warn("No " + _TYPE + " found", e);
		}
		String toSerializeJSONOBJ = thJsonObject.toString();
		String jsonPath = null;
		try {
			jsonPath = String.format("%s.%s", JSON_$_POINTER, _BBOX);
			HashMap<String, Object> bbox = JsonPath.using(config).parse(toSerializeJSONOBJ).read(jsonPath,
					HashMap.class);
			BBOXDV bboxDV = new BBOXDV(bbox);
			gsdiLayer.setBbox(bboxDV);
			LOG.debug(_BBOX + " is: " + bboxDV);
		} catch (Exception e) {
			LOG.warn(jsonPath + " error: ", e);
		}
		try {
			jsonPath = String.format("%s.%s", JSON_$_POINTER, _OGC_LINKS);
			String jsonString = JsonPath.using(config).parse(toSerializeJSONOBJ).read(jsonPath).toString();
			Gson gson = new Gson();
			HashMap<String, String> map = gson.fromJson(jsonString, HashMap.class);
			gsdiLayer.setOgcLinks(map);
			LOG.debug(_OGC_LINKS + " are: " + map);
		} catch (Exception e) {
			LOG.warn(jsonPath + " error: ", e);
		}

		try {
			String wmsLink = gsdiLayer.getWMSLink();
			if (wmsLink != null) {
				String layerName = URLParserUtil.extractValueOfParameterFromURL("layers", wmsLink);
				gsdiLayer.setLayerName(layerName);
			}
		} catch (Exception e) {
			LOG.warn(jsonPath + " error: ", e);
		}

		LOG.debug("converLayer returning: " + gsdiLayer);
		return gsdiLayer;
	}
	
	
	/**
	 * Checks if is accessible section according to policy.
	 *
	 * @param section         the section
	 * @param sectionJSONPath the section JSON path
	 * @param myLogin         the my login
	 * @return true, if is accessible section according to policy
	 */
	private static boolean isAccessibleSectionAccordingToPolicy(Document section, String filesetPath,
			String myLogin) {
		LOG.debug("isAccessibleSectionAccordingToPolicy called");
		boolean isAccessible = true;

		// Skipping the root, going to check the access_policy of subsections
		//if (sectionJSONPath.compareTo(JSON_$_POINTER) != 0) {
		isAccessible = checkAccessPolicy(section.toJson(), filesetPath, myLogin);
		//}

		return isAccessible;
	}


	/**
	 * Check access policy.
	 *
	 * @param sectionDocumentJSON the section document JSON
	 * @param filesetPath the fileset path. eg. $.filest | $.filesetIta | $.filesetEng, etc.
	 * @param myLogin             the my login
	 * @return true, if successful
	 */
	private static boolean checkAccessPolicy(String sectionDocumentJSON, String filesetPath, String myLogin) {
		LOG.debug("checkAccessPolicy called");
		// CHECKING THE POLICY
		// see ticket #24390
		// First reading the access policy from the fileset*
		//String _policy = getAccessPolicy(sectionDocumentJSON);
		Access _access = getAccessPolicyObject(sectionDocumentJSON, filesetPath);
		boolean isAccessible = true;
		try {
			String _policy= _access!=null? _access.getPolicy().name():null;
			isAccessible = GeportalCheckAccessPolicy.isAccessible(_policy, myLogin);
		} catch (Exception e) {
			LOG.error("AccessPolicy not found. Check OK");
		}
		
		//Just to print
		int toEndString = 0;
		try {
			toEndString = sectionDocumentJSON.length()/3;
		}catch (Exception e) {
			//Silent
			toEndString = 0;
		}
		
		if(LOG.isDebugEnabled()) {
			LOG.debug("It is {} accessible the section {}", isAccessible, sectionDocumentJSON);
		}
		LOG.info("It is {} accessible the section {}", isAccessible, sectionDocumentJSON.substring(0,toEndString)+"...");
		return isAccessible;
	}
	
	/**
	 * Gets the access policy object.
	 *
	 * @param theSectionDoc the the section doc
	 * @param filesetPath   the fileset path
	 * @return the access policy object
	 */
	private static Access getAccessPolicyObject(String theSectionDoc, String filesetPath) {
		LOG.debug("getAccessPolicyObject called");
		// CHECKING THE POLICY
		// see ticket #24390

		// First reading the access policy from the fileset*
		Configuration configuration = Configuration.builder().jsonProvider(new JsonOrgJsonProvider()).build();

		// Searching the _access under the fileset field...
		String accessPolicyPath = filesetPath + "._access";
		Access _access = readSectionObject(configuration, theSectionDoc, accessPolicyPath, Access.class);

		// If policy does not exist under the fileset field, searching it at first level
		if (_access == null) {
			accessPolicyPath = JSON_$_POINTER + "._access";
			LOG.info(accessPolicyPath + " not found trying to read from: " + accessPolicyPath);
			_access = readSectionObject(configuration, theSectionDoc, accessPolicyPath, Access.class);
		}

		if (_access == null) {
			LOG.info(accessPolicyPath + " not found trying to read from metadata: " + JSON_$_POINTER);
			_access = ConvertToDataServiceModel.getAccessFromDocumentSection(theSectionDoc, JSON_$_POINTER);
		}

		LOG.info("The _access is {} for the filesetPath {}", _access, filesetPath);
		return _access;
	}
	
	/**
	 * Sanitize document value.
	 *
	 * @param toDoc               the to doc
	 * @param fieldLabel          the field label
	 * @param theObjectFieldValue the the object field value
	 * @return the document
	 */
	private static Document sanitizeDocumentValue(Document toDoc, String fieldLabel, Object theObjectFieldValue) {

		if (theObjectFieldValue != null) {
			if (theObjectFieldValue instanceof String) {
				String toString = (String) theObjectFieldValue;
				if (toString != null && !toString.isEmpty()) {
					toDoc.append(fieldLabel, toString.trim());
				} else {
					LOG.debug("Skipping String field " + fieldLabel + " its value is null or empty");
				}

			} else if (theObjectFieldValue instanceof ArrayList) {
				ArrayList toArrayList = (ArrayList) theObjectFieldValue;
				if (toArrayList != null && !toArrayList.isEmpty()) {
					toDoc.append(fieldLabel, theObjectFieldValue);
				} else {
					LOG.debug("Skipping ArrayList field " + fieldLabel + " its value is null or empty");
				}
			} else {
				toDoc.append(fieldLabel, theObjectFieldValue);
			}
		} else {
			LOG.debug("Skipping field " + fieldLabel + " its value is null or empty");
		}

		return toDoc;
	}

	/**
	 * Read section object.
	 *
	 * @param <T>                 the generic type
	 * @param configuration       the configuration
	 * @param sectionDocumentJSON the section document JSON
	 * @param accessPolicyPath    the access policy path
	 * @param theClass            the the class
	 * @return the t
	 */
	private static <T> T readSectionObject(com.jayway.jsonpath.Configuration configuration, String sectionDocumentJSON,
			String accessPolicyPath, Class<T> theClass) {

		T _theObject = null;

		LOG.debug("Reading access at {} into section document {}", accessPolicyPath, sectionDocumentJSON);
		try {
			JsonPath theSectionPolycJsonPath = JsonPath.compile(accessPolicyPath);
			String _objectString = theSectionPolycJsonPath.read(sectionDocumentJSON, configuration).toString();
			LOG.info("Read at {} the _objectString {}", accessPolicyPath, _objectString);
			if (_objectString != null) {
				_theObject = Serialization.read(_objectString, theClass);
			}

		} catch (Exception e) {
			LOG.warn("Access policy not found in: " + accessPolicyPath);
		}

		return _theObject;

	}

	/**
	 * Pretty print JSON.
	 *
	 * @param jsonString the json string
	 * @return the string
	 */
	private static String prettyPrintJSON(String jsonString) {

		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		JsonObject jsonObject = new JsonParser().parse(jsonString).getAsJsonObject();
		return gson.toJson(jsonObject);
	}

	/**
	 * Pretty print project view.
	 *
	 * @param projectView the project view
	 */
	public static void prettyPrintProjectView(ProjectView projectView) {

		for (SectionView section : projectView.getListSections()) {
			System.out.println("\n\n###### Section Title: " + section.getSectionTitle() + " ######");
			int i = 1;
			for (SubDocumentView subDocument : section.getListSubDocuments()) {
				System.out.println("## " + SubDocumentView.class.getSimpleName() + " n." + i);
				System.out.println("***** Metadata");
				System.out.println(prettyPrintJSON(subDocument.getMetadataAsJSON()));
				System.out.println("***** Files");
				if (subDocument.getListFiles() != null) {
					for (FilesetDV filesetDV : subDocument.getListFiles()) {
						System.out.println("******* File Fileset name: " + filesetDV.getGcubeProfileFieldName());
						for (PayloadDV payload : filesetDV.getListPayload()) {
							System.out.println("********* Payload: " + payload);
						}
					}
				}
				System.out.println("***** Images");
				if (subDocument.getListImages() != null) {
					for (FilesetDV filesetDV : subDocument.getListImages()) {
						System.out.println("******* Image Fileset name: " + filesetDV.getGcubeProfileFieldName());
						for (PayloadDV payload : filesetDV.getListPayload()) {
							System.out.println("********* Payload: " + payload);
						}
					}
				}
				System.out.println("***** Layers");
				if (subDocument.getListLayers() != null) {
					for (GCubeSDIViewerLayerDV layer : subDocument.getListLayers()) {
						System.out.println("******* Layer type: " + layer.getType());
						System.out.println("******* Layer: " + layer);
					}
				}
				i++;
			}

		}
	}

	/**
	 * Pretty print project view.
	 *
	 * @param projectEdit the project edit
	 */
	public static void prettyPrintProjectEdit(ProjectEdit projectEdit) {

		for (MetaDataProfileBeanExt mpb : projectEdit.getTheProfileBeans()) {
			System.out.println("\n\n###### Title: " + mpb.getTitle() + " - Type: " + mpb.getType() + " ######");
			int i = 1;
			for (MetadataFieldWrapper fieldWrapper : mpb.getMetadataFields()) {
				System.out.println("## " + MetadataFieldWrapper.class.getSimpleName() + " n." + i);
				System.out.println("***** Metadata");
				// System.out.println(mfw);
				System.out.println("\tfieldId: " + fieldWrapper.getFieldId() + ", fieldName: "
						+ fieldWrapper.getFieldName() + ", CurrentValues: " + fieldWrapper.getCurrentValues());
				i++;
			}
			i = 1;
			System.out.println("***** Files");
			for (FilesetDV fileSet : mpb.getListFileset()) {
				System.out.println(
						"## " + FilesetDV.class.getSimpleName() + " n." + i + " has name: " + fileSet.getGcubeProfileFieldName());

				if (fileSet.getListPayload() != null) {
					int j = 0;
					for (PayloadDV payload : fileSet.getListPayload()) {
						System.out.println("\t" + ++j + ") " + payload);
					}
				}
				i++;
			}

		}
	}

}
