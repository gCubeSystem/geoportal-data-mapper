package org.gcube.application.geoportaldatamapper;

import java.util.Arrays;
import java.util.List;

public class ImageDetector {

	public static enum COMMON_IMAGES_FORMAT {
		gif, png, jpeg, jpg, bmp, svg, avif, webp
	}

	/**
	 * The Class ImageDetector.
	 *
	 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
	 * 
	 *         Oct 18, 2022
	 */

	/**
	 * Gets the names.
	 *
	 * @param e the e
	 * @return the names
	 */
	private static String[] getNames(Class<? extends Enum<?>> e) {
		return Arrays.stream(e.getEnumConstants()).map(Enum::name).toArray(String[]::new);
	}

	public static List<String> listFormatImages;

	static {

		String[] arrayImgs = ImageDetector.getNames(COMMON_IMAGES_FORMAT.class);
		listFormatImages = Arrays.asList(arrayImgs);

	}

	/**
	 * Checks if is image.
	 *
	 * @param mimeType the mime type
	 * @return true, if is image
	 */
	public static boolean isImage(String mimeType) {
		if (mimeType == null || mimeType.isEmpty())
			return false;

		String inputImageFormat = mimeType.replaceAll("image/", "");

		return listFormatImages.contains(inputImageFormat);
	}

}
