package org.gcube.application.geoportaldatamapper.reader;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ServiceAccessPoint {
	String url;
	String apiTokenName;
	String apiTokenPwd;

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ServiceAccessPoint [url=");
		builder.append(url);
		builder.append(", apiTokenName=");
		builder.append(apiTokenName);
		builder.append(", apiTokenPwd=");
		builder.append("MASKED_PWD");
		builder.append("]");
		return builder.toString();
	}

}