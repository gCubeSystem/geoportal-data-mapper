package org.gcube.application.geoportaldatamapper.reader;

import static org.gcube.resources.discovery.icclient.ICFactory.clientFor;
import static org.gcube.resources.discovery.icclient.ICFactory.queryFor;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.gcube.common.encryption.StringEncrypter;
import org.gcube.common.resources.gcore.ServiceEndpoint;
import org.gcube.common.resources.gcore.ServiceEndpoint.AccessPoint;
import org.gcube.common.scope.api.ScopeProvider;
import org.gcube.resources.discovery.client.api.DiscoveryClient;
import org.gcube.resources.discovery.client.queries.api.SimpleQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class MapBoxEndpointReader.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Dec 20, 2023
 */
public class MapBoxEndpointReader {

	private static Logger LOG = LoggerFactory.getLogger(MapBoxEndpointReader.class);

	private static final String SE_PROFILE_NAME = "API_MapBox";
	private static final String SE_CATEGORY_NAME = "OnlineService";

	/**
	 * Gets the map box endpoint.
	 *
	 * @return the map box endpoint
	 * @throws Exception the exception
	 */
	public static List<ServiceAccessPoint> getMapBoxEndpoint() throws Exception {

		LOG.info("Searching SE in the scope: " + ScopeProvider.instance.get() + " with profile name: " + SE_PROFILE_NAME
				+ " and category name: " + SE_CATEGORY_NAME);

		List<ServiceAccessPoint> listSAP = new ArrayList<ServiceAccessPoint>();

		SimpleQuery query = queryFor(ServiceEndpoint.class);
		query.addCondition("$resource/Profile/Name/text() eq '" + SE_PROFILE_NAME + "'");
		query.addCondition("$resource/Profile/Category/text() eq '" + SE_CATEGORY_NAME + "'");

		DiscoveryClient<ServiceEndpoint> client = clientFor(ServiceEndpoint.class);
		List<ServiceEndpoint> resources = client.submit(query);

		if (resources.size() > 0)
			LOG.info("The query returned " + resources.size() + " ServiceEndpoint/s");
		else
			throw new RuntimeException("ServiceEndpoint not found. Searching for profile name: " + SE_PROFILE_NAME
					+ " and category name: " + SE_CATEGORY_NAME + ", in the scope: " + ScopeProvider.instance.get());

		ServiceEndpoint se = resources.get(0);
		Collection<AccessPoint> theAccessPoints = se.profile().accessPoints().asCollection();
		for (AccessPoint accessPoint : theAccessPoints) {
			String url = accessPoint.address();
			String apiTokenName = accessPoint.username();
			String apiTokenPwd = accessPoint.password();
			LOG.debug("Found API_TOKEN name: " + apiTokenName + ", encrypted token: " + apiTokenPwd);
			// decrypting the pwd
			try {
				if (isValorized(apiTokenPwd)) {
					apiTokenPwd = StringEncrypter.getEncrypter().decrypt(apiTokenPwd);
					LOG.debug("Token decrypted is: " + apiTokenPwd.substring(0, apiTokenPwd.length() / 2)
							+ "_MASKED_TOKEN_");
				}
			} catch (Exception e) {
				throw new RuntimeException("Error on decrypting the pwd: ", e);
			}

			ServiceAccessPoint ap = new ServiceAccessPoint();
			ap.setUrl(url);
			ap.setApiTokenName(apiTokenName);
			ap.setApiTokenPwd(apiTokenPwd);
			listSAP.add(ap);
		}
		
		LOG.info("SE in the scope: " + ScopeProvider.instance.get() + " with profile name: " + SE_PROFILE_NAME
				+ " and category name: " + SE_CATEGORY_NAME + "is returning: "+listSAP);
		return listSAP;

	}

	/**
	 * Checks if is valorized.
	 *
	 * @param value the value
	 * @return true, if is valorized
	 */
	private static boolean isValorized(String value) {
		if (value != null && !value.isEmpty()) {
			return true;
		}
		return false;
	}
}
