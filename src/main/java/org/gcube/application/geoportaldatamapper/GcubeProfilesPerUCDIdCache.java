/**
 *
 */

package org.gcube.application.geoportaldatamapper;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.gcube.application.geoportal.common.model.useCaseDescriptor.HandlerDeclaration;
import org.gcube.application.geoportal.common.model.useCaseDescriptor.UseCaseDescriptor;
import org.gcube.application.geoportalcommon.ConvertToDataValueObjectModel;
import org.gcube.application.geoportalcommon.geoportal.GeoportalClientCaller;
import org.gcube.application.geoportalcommon.geoportal.UseCaseDescriptorCaller;
import org.gcube.application.geoportalcommon.shared.geoportal.ConfigurationDV;
import org.gcube.application.geoportalcommon.shared.geoportal.config.GcubeProfileDV;
import org.gcube.application.geoportalcommon.shared.geoportal.ucd.GEOPORTAL_CONFIGURATION_TYPE;
import org.gcube.application.geoportalcommon.shared.geoportal.ucd.GEOPORTAL_DATA_HANDLER;
import org.gcube.application.geoportalcommon.shared.geoportal.ucd.HandlerDeclarationDV;
import org.gcube.common.scope.api.ScopeProvider;
import org.gcube.portlets.widgets.mpformbuilder.server.MetadataDiscovery;
import org.gcube.portlets.widgets.mpformbuilder.shared.metadata.MetaDataProfileBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.cache.RemovalListener;
import com.google.common.cache.RemovalNotification;

/**
 * The Class GcubeProfilesPerUCDIdCache.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 * Oct 12, 2022
 */
public class GcubeProfilesPerUCDIdCache {

	private static Logger LOG = LoggerFactory.getLogger(GcubeProfilesPerUCDIdCache.class);

	private static LoadingCache<String, LinkedHashMap<String, List<GcubeProfilesMetadataForUCD>>> gCubeProfilesPerProfileIDCache;

	static {

		CacheLoader<String, LinkedHashMap<String, List<GcubeProfilesMetadataForUCD>>> loader = new CacheLoader<String, LinkedHashMap<String, List<GcubeProfilesMetadataForUCD>>>() {
			@Override
			public LinkedHashMap<String, List<GcubeProfilesMetadataForUCD>> load(String scope) throws Exception {
				LOG.info("Loading the cache for scope: " + scope);
				return loadGcubeProfilesForUCDIdInTheScope(scope);
			}
		};

		RemovalListener<String, LinkedHashMap<String, List<GcubeProfilesMetadataForUCD>>> removalListener = new RemovalListener<String, LinkedHashMap<String, List<GcubeProfilesMetadataForUCD>>>() {
			public void onRemoval(
					RemovalNotification<String, LinkedHashMap<String, List<GcubeProfilesMetadataForUCD>>> removal) {
				LOG.info(GcubeProfilesPerUCDIdCache.class.getSimpleName() + " cache expired");
			}
		};

		gCubeProfilesPerProfileIDCache = CacheBuilder.newBuilder().maximumSize(1000)
				.expireAfterWrite(30, TimeUnit.MINUTES).removalListener(removalListener).build(loader);

		LOG.info("cache instancied");
	}

	/**
	 * Gets the.
	 *
	 * @param scope the scope
	 * @return the geonetwork instance
	 * @throws Exception 
	 */
	public static LinkedHashMap<String, List<GcubeProfilesMetadataForUCD>> get(String scope) throws Exception {
		LOG.info("GcubeProfilesPerUCDIdCache get - called in the scope: " + scope);
		LinkedHashMap<String, List<GcubeProfilesMetadataForUCD>> map = gCubeProfilesPerProfileIDCache.get(scope);
		LOG.info("GcubeProfilesPerUCDIdCache returning map null? " + (map==null));
		return map;
	}

	/**
	 * Load geonetwork instance.
	 *
	 * @param scope the scope
	 * @return the linked hash map
	 * @throws Exception the exception
	 */
	private static LinkedHashMap<String, List<GcubeProfilesMetadataForUCD>> loadGcubeProfilesForUCDIdInTheScope(
			String scope) throws Exception {
		LOG.info("loadGcubeProfilesForUCDIdInTheScope called in the scope: " + scope);

		LinkedHashMap<String, List<GcubeProfilesMetadataForUCD>> linkedMap_UCDId_gCubeProfiles = new LinkedHashMap<String, List<GcubeProfilesMetadataForUCD>>();
		try {
			ScopeProvider.instance.set(scope);
			UseCaseDescriptorCaller clientUCD = GeoportalClientCaller.useCaseDescriptors();
			List<UseCaseDescriptor> listUCDs = clientUCD.getList();
			LOG.debug("listUCDs: " + listUCDs);

			for (UseCaseDescriptor ucd : listUCDs) {

				LOG.info("Loaded UCD for ID: " + ucd.getId());
				String profileID = ucd.getId();
				GEOPORTAL_DATA_HANDLER theHandler = GEOPORTAL_DATA_HANDLER.geoportal_data_entry;
				List<HandlerDeclaration> handlers = ucd.getHandlersByType(theHandler.getType());

				if (handlers.size() == 0) {
					LOG.warn("No handler " + theHandler + "found into UCD " + ucd.getId() + ", continue...");
					continue;
				}

				// Loading Handler gcube_profiles
				HandlerDeclaration dataEntryHandler = handlers.get(0);
				HandlerDeclarationDV handlerGcubeProfiles = ConvertToDataValueObjectModel
						.toHandlerDeclarationDV(dataEntryHandler, ucd, GEOPORTAL_CONFIGURATION_TYPE.gcube_profiles);

				LOG.debug("Handler " + GEOPORTAL_CONFIGURATION_TYPE.gcube_profiles + " for PROFILE_ID: " + ucd.getId());
				LOG.debug("" + handlerGcubeProfiles);

				ConfigurationDV<?> config = handlerGcubeProfiles.getConfiguration();
				// List of gCube Profiles defined in the UCD
				List<GcubeProfileDV> listGcubeProfiles = (List<GcubeProfileDV>) config.getConfiguration();
				LOG.debug("List of GcubeProfileDV are: " + listGcubeProfiles);

				List<GcubeProfilesMetadataForUCD> listProfilesBean = new ArrayList<GcubeProfilesMetadataForUCD>();
				// Loading Metadata Profile from IS
				//MetadataProfileFormBuilderServiceImpl metaProfileBUilder = new MetadataProfileFormBuilderServiceImpl();
				List<MetaDataProfileBean> listProfiles = null;
				for (GcubeProfileDV gcubeProfileDV : listGcubeProfiles) {
					ScopeProvider.instance.set(scope);
					GcubeProfilesMetadataForUCD gCubeProfileMetadataForUCD = new GcubeProfilesMetadataForUCD();
//					List<MetaDataProfileBean> listProfiles = metaProfileBUilder.getProfilesInTheScopeForName(scope,
//							gcubeProfileDV.getGcubeSecondaryType(), gcubeProfileDV.getGcubeName());
					
					try {
						LOG.debug("Evaluated scope is  " + scope);
						listProfiles = MetadataDiscovery.getMetadataProfilesList(scope, gcubeProfileDV.getGcubeSecondaryType(), gcubeProfileDV.getGcubeName());
					} catch (Exception e) {
						LOG.error("Failed to retrieve profiles for scope " +scope, e);
						throw e;
					}

					String key = gcubeProfileDV.getGcubeSecondaryType() + gcubeProfileDV.getGcubeName();
					LOG.debug("for key: " + key + " readi profiles: " + listGcubeProfiles);
					gCubeProfileMetadataForUCD.setGcubeProfile(gcubeProfileDV);
					gCubeProfileMetadataForUCD.setListMetadataProfileBean(listProfiles);
					listProfilesBean.add(gCubeProfileMetadataForUCD);

				}
				linkedMap_UCDId_gCubeProfiles.put(ucd.getId(), listProfilesBean);

				if (LOG.isDebugEnabled()) {
					for (String key : linkedMap_UCDId_gCubeProfiles.keySet()) {
						LOG.debug("For key '" + key + "' got profiles: " + linkedMap_UCDId_gCubeProfiles.get(key));
					}
				}

			}

		} catch (Exception e) {
			String erroMsg = "Error occurred on preloadgCubeProfilesForUCDs: ";
			LOG.error(erroMsg, e);
		}
		
		LOG.info("GcubeProfilesPerUCDIdCache loaded with " + linkedMap_UCDId_gCubeProfiles.size() + " item/s");
		return linkedMap_UCDId_gCubeProfiles;
	}
}
