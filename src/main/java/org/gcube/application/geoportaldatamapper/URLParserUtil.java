package org.gcube.application.geoportaldatamapper;

import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * The Class URLUtil.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * 
 *         Oct 29, 2020
 */
public class URLParserUtil {

	/**
	 * Adds the parameter to query string.
	 *
	 * @param key             the key
	 * @param value           the value
	 * @param prefixAmpersand the prefix ampersand
	 * @param suffixAmpersand the suffix ampersand
	 * @return the string
	 */
	public static String addParameterToQueryString(String key, String value, boolean prefixAmpersand,
			boolean suffixAmpersand) {

		String queryParameter = "";

		if (prefixAmpersand)
			queryParameter += "&";

		queryParameter += key + "=" + value;

		if (suffixAmpersand)
			queryParameter += "&";

		return queryParameter;

	}
	
	/**
	 * Sets the value of parameter.
	 *
	 * @param wmsParam       the wms param
	 * @param wmsLink        the wms link
	 * @param newValue       the new value
	 * @param addIfNotExists the add if not exists
	 * @return the string
	 */
	public static String setValueOfParameter(String wmsParam, String wmsLink, String newValue, boolean addIfNotExists) {
		String toLowerWmsLink = wmsLink.toLowerCase();
		String toLowerWmsParam = wmsParam.toLowerCase();

		int index = toLowerWmsLink.indexOf(toLowerWmsParam + "="); // END WITH CHAR "=" TO BE SURE THAT IT IS A
																	// PARAMETER
		if (index > -1) {
			int indexStartValue = index + toLowerWmsParam.length() + 1; // add +1 for char '='
			int indexOfSeparator = toLowerWmsLink.indexOf("&", indexStartValue); // GET THE FIRST "&" STARTING FROM
																					// INDEX VALUE
//			logger.trace("indexOfSeparator index of "+wmsParam+ " is: "+indexOfSeparator);
			int indexEndValue = indexOfSeparator != -1 ? indexOfSeparator : toLowerWmsLink.length();
//			logger.trace("end: "+indexEndValue);
			return wmsLink.substring(0, indexStartValue) + newValue
					+ wmsLink.substring(indexEndValue, wmsLink.length());
		} else if (addIfNotExists) {
			wmsLink += "&" + wmsParam + "=" + newValue;
		}
//		logger.trace("return value: "+value);
		return wmsLink;
	}

	/**
	 * Extract value of parameter from URL.
	 *
	 * @param paramName the param name
	 * @param url       the url
	 * @return the string
	 */
	public static String extractValueOfParameterFromURL(String paramName, String url) {
		int index = url.toLowerCase().indexOf(paramName.toLowerCase() + "="); // ADDING CHAR "=" IN TAIL TO BE SURE THAT
																				// IT
																				// IS A PARAMETER
		String value = "";
		if (index > -1) {

			int start = index + paramName.length() + 1; // add +1 for char '='
			String sub = url.substring(start, url.length());
			int indexOfSeparator = sub.indexOf("&");
			int end = indexOfSeparator != -1 ? indexOfSeparator : sub.length();
			value = sub.substring(0, end);
		} else
			return null;

		return value;
	}

	
	/**
	 * Split query.
	 *
	 * @param url the url
	 * @return the map
	 * @throws UnsupportedEncodingException the unsupported encoding exception
	 */
	public static Map<String, List<String>> splitQuery(URL url) throws UnsupportedEncodingException {
		final Map<String, List<String>> query_pairs = new LinkedHashMap<String, List<String>>();
		final String[] pairs = url.getQuery().split("&");
		for (String pair : pairs) {
			final int idx = pair.indexOf("=");
			final String key = idx > 0 ? URLDecoder.decode(pair.substring(0, idx), "UTF-8") : pair;
			if (!query_pairs.containsKey(key)) {
				query_pairs.put(key, new LinkedList<String>());
			}
			final String value = idx > 0 && pair.length() > idx + 1
					? URLDecoder.decode(pair.substring(idx + 1), "UTF-8")
					: null;
			query_pairs.get(key).add(value);
		}
		return query_pairs;
	}

}
