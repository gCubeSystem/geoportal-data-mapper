package org.gcube.application.geoportaldatamapper.exporter.beans;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Data;

@XmlRootElement(name = "credits")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class Credits {

	String header;
	String footer;

}
