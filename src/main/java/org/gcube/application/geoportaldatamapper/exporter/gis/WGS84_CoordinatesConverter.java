package org.gcube.application.geoportaldatamapper.exporter.gis;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * The Class WGS84_CoordinatesConverter.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Dec 18, 2023
 */
public class WGS84_CoordinatesConverter {

	private static double EARTH_RADIUS = 6378137.0;

	/**
	 * From 4326 To epsg 3857.
	 *
	 * @param coordinates the coordinates as long/lat
	 * @return the list
	 */
	public static List<Double> toEpsg3857(List<Double> coordinates) {
		double x = Math.toRadians(coordinates.get(0)) * EARTH_RADIUS;
		double y = Math.log(Math.tan(Math.PI / 4 + Math.toRadians(coordinates.get(1)) / 2)) * EARTH_RADIUS;
		return Arrays.asList(x, y);
	}

	/**
	 * From 3857 To 4326.
	 *
	 * @param coordinates the coordinates as long/lat
	 * @return the list
	 */
	public static List<Double> to4326(List<Double> coordinates) {
		double lat = Math.toDegrees(Math.atan(Math.exp(coordinates.get(1) / EARTH_RADIUS)) * 2 - Math.PI / 2);
		double lng = Math.toDegrees(coordinates.get(0) / EARTH_RADIUS);
		return Arrays.asList(lng, lat);
	}

	/**
	 * From 4326 To EPSG 3857 BBOX.
	 *
	 * @param bbox the bbox
	 * @return the list
	 */
	public static List<Double> toEPSG3857_BBOX(String bbox) {
		String[] parts = bbox.split(","); // remove spaces and split on ,
		double minX = Double.parseDouble(parts[0]);
		double minY = Double.parseDouble(parts[1]);
		double maxX = Double.parseDouble(parts[2]);
		double maxY = Double.parseDouble(parts[3]);
		List<Double> l1 = toEpsg3857(Arrays.asList(minX, minY));
		List<Double> l2 = toEpsg3857(Arrays.asList(maxX, maxY));
		// return String.format("%s,%s,%s,%s", l1.get(0), l1.get(1), l2.get(0),
		// l2.get(1));
		return Arrays.asList(l1.get(0), l1.get(1), l2.get(0), l2.get(1));
	}

	/**
	 * To string.
	 *
	 * @param bbox the bbox
	 * @return the string
	 */
	public static String toString(List<Double> bbox) {
		return String.format("%s,%s,%s,%s", bbox.get(0), bbox.get(1), bbox.get(2), bbox.get(3));
	}
	
	/**
	 * From 3857 To EPSG 4326 BBOX.
	 *
	 * @param bbox the bbox
	 * @return the list
	 */
	public static List<Double> toEPSG4326_BBOX(String bbox) {
		String[] parts = bbox.split(","); // remove spaces and split on ,
		double minX = Double.parseDouble(parts[0]);
		double minY = Double.parseDouble(parts[1]);
		double maxX = Double.parseDouble(parts[2]);
		double maxY = Double.parseDouble(parts[3]);
		List<Double> l1 = to4326(Arrays.asList(minX, minY));
		List<Double> l2 = to4326(Arrays.asList(maxX, maxY));
		// return String.format("%s,%s,%s,%s", l1.get(0), l1.get(1), l2.get(0),
		// l2.get(1));
		return Arrays.asList(l1.get(0), l1.get(1), l2.get(0), l2.get(1));
	}

	/**
	 * Diff BBOX.
	 *
	 * @param bbox the bbox
	 * @return the array list
	 */
	public static ArrayList<Double> diffBBOX(String bbox) {
		String[] parts = bbox.replaceAll("\\s", "").split(","); // remove spaces and split on ,

		double minX = Double.parseDouble(parts[0]);
		double minY = Double.parseDouble(parts[1]);
		double maxX = Double.parseDouble(parts[2]);
		double maxY = Double.parseDouble(parts[3]);

		double width = maxX - minX;
		double height = maxY - minY;

		ArrayList size = new ArrayList<Double>();
		size.add(width);
		size.add(height);

		return size;
	}
}
