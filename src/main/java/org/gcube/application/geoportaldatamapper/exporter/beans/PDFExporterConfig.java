package org.gcube.application.geoportaldatamapper.exporter.beans;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Data;

@XmlRootElement(name = "pdf_exporter_config")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class PDFExporterConfig {

	private Credits credits;

	@XmlElement(required = false)
	private Watermarker watermarker;

	@XmlElement(required = false)
	private PageNumber pagenumber;

	@XmlElement(name = "timestamp_created_at", required = false)
	private Boolean timestampCreatedAt;

}
