package org.gcube.application.geoportaldatamapper.exporter;

import static org.gcube.resources.discovery.icclient.ICFactory.client;

import java.io.StringReader;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.gcube.application.geoportaldatamapper.exporter.beans.PDFExporterConfig;
import org.gcube.common.resources.gcore.utils.XPathHelper;
import org.gcube.common.scope.api.ScopeProvider;
import org.gcube.resources.discovery.client.api.DiscoveryClient;
import org.gcube.resources.discovery.client.queries.api.Query;
import org.gcube.resources.discovery.client.queries.impl.QueryBox;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;

/**
 * Retrieves available 'PDF Exporter Config' from a single Generic Resource -
 * secondary Type : "CMS" - name : "PDF_Exporter_Config".
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Nov 20, 2023
 */
public class PDFExporterConfigProvider {

	private static final String GR_SECONDARY_TYPE = "CMS";
	private static final String GR_NAME = "PDF_Exporter_Config";

	private static final String RESOURCE_PROFILE_BODY = "/Resource/Profile/Body";
	private static final String PDF_EXPORTER_CONFIG = "pdf_exporter_config";

	private static Logger LOG = LoggerFactory.getLogger(PDFExporterConfigProvider.class);

	/**
	 * Instantiates a new exporter file provider.
	 */
	public PDFExporterConfigProvider() {
	}

	/**
	 * Read configs from GR.
	 *
	 * @return the PDF exporter project config
	 * @throws Exception the exception
	 */
	protected PDFExporterConfig readConfigsFromGR() throws Exception {

		String queryString = getGcubeGenericQueryString(GR_SECONDARY_TYPE, GR_NAME);
		LOG.info("Trying to perform query: " + queryString);

		String pdf_exporter_onfig = null;
		PDFExporterConfig epc = null;
		String scope = ScopeProvider.instance.get();

		if (scope == null)
			throw new Exception("Set the scope into " + ScopeProvider.class.getName());

		try {

			LOG.info("Trying to fetch GenericResource in the scope: " + scope + ", SecondaryType: " + GR_SECONDARY_TYPE
					+ ", Name: " + GR_NAME);
			Query q = new QueryBox(queryString);
			DiscoveryClient<String> client = client();
			List<String> appProfile = client.submit(q);
//			String item_fields = "";

			if (appProfile == null || appProfile.size() == 0)
				LOG.warn("No GenericResource found in the scope: " + scope + ", SecondaryType: " + GR_SECONDARY_TYPE
						+ ", Name: " + GR_NAME);
			else {
				String elem = appProfile.get(0);
				DocumentBuilder docBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
				Document doc = docBuilder.parse(new InputSource(new StringReader(elem)));

				XPathHelper helper = new XPathHelper(doc.getDocumentElement());

				List<String> currValue = null;
				String xPathExp = RESOURCE_PROFILE_BODY + "/" + PDF_EXPORTER_CONFIG;
				currValue = helper.evaluate(xPathExp);
				// System.out.println("currValue is: "+currValue);
				if (currValue != null && currValue.size() > 0) {
					pdf_exporter_onfig = currValue.get(0);
				}

				if (pdf_exporter_onfig != null) {
					try {
						epc = fromXml(pdf_exporter_onfig, PDFExporterConfig.class);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				LOG.info("returning: " + epc);
				return epc;
			}

		} catch (Exception e) {
			LOG.error("Error while trying to read the  GenericResource with SecondaryType " + GR_SECONDARY_TYPE
					+ "and name " + GR_NAME + " from the scope " + scope, e);

		} finally {

		}
		return null;
	}

	/**
	 * Gets the gcube generic query string.
	 *
	 * @param secondaryType the secondary type
	 * @param resourceName  the resource name
	 * @return the gcube generic query string
	 */
	public static String getGcubeGenericQueryString(String secondaryType, String resourceName) {

		return "for $profile in collection('/db/Profiles/GenericResource')//Resource "
				+ "where $profile/Profile/SecondaryType/string() eq '" + secondaryType
				+ "' and  $profile/Profile/Name/string() " + " eq '" + resourceName + "'" + "return $profile";
	}

	/**
	 * Deserializes a XML text to Object.
	 *
	 * @param <T>   the generic type
	 * @param xml   the xml
	 * @param clazz the clazz
	 * @return the t
	 */
	public <T> T fromXml(String xml, Class<T> clazz) {
		try {
			JAXBContext context = JAXBContext.newInstance(clazz);
			Unmarshaller unmarshaller = context.createUnmarshaller();
			//unmarshaller.setAdapter(new NormalizedStringAdapter());

			Object o = unmarshaller.unmarshal(new StringReader(xml));
			return clazz.cast(o);
		} catch (Exception e) {
			throw new IllegalStateException("Error while deserializing a XML text to Object of type " + clazz, e);
		}
	}
}
