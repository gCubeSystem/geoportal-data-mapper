package org.gcube.application.geoportaldatamapper.exporter.gis;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Recalculate_WGS84_Offset {

	private String wmsVersion;
	private String sourceBBOX;
	private double bboxOffset;
	private String bboxWithOffset;

	public Double expWidth = 500.0;
	public Double expHeight = 500.0;

	private int aspectratioWidth;
	private int aspectratioHeight;
	private double ratio_Width_Height;

	private Logger LOG = LoggerFactory.getLogger(Recalculate_WGS84_Offset.class);

	public Recalculate_WGS84_Offset(String wmsVersion, String bbox, double bboxOffset) {
		this.wmsVersion = wmsVersion;
		this.sourceBBOX = bbox;
		this.bboxOffset = bboxOffset;
		LOG.trace("sourceBBOX: " + sourceBBOX);
		this.bboxWithOffset = BBOXConverter.bboxWithOffset(wmsVersion, sourceBBOX, this.bboxOffset);

		// Calculating bounding box dimension
		List<Double> bbox3857_BB = WGS84_CoordinatesConverter.toEPSG3857_BBOX(bboxWithOffset);
		String bbox3857 = WGS84_CoordinatesConverter.toString(bbox3857_BB);
		LOG.debug("toEPSG3857_BBOX: " + bbox3857);
		ArrayList<Double> diff = WGS84_CoordinatesConverter.diffBBOX(bbox3857);
		LOG.debug("BoudingBox [width dimension, height dimension]: " + diff);
		double width = diff.get(0);
		double height = diff.get(1);
		// Calculating width/height ratio
		ratio_Width_Height = width / height;
		LOG.trace("bbox width: " + width);
		LOG.trace("bbox heigth: " + height);
		LOG.debug("bbox ratio: " + ratio_Width_Height);
		
		//resizing width according to ratio width/height
		expWidth = expWidth * ratio_Width_Height;
		
		this.aspectratioWidth = expWidth.intValue();
		LOG.debug("aspectratioWidth: " + aspectratioWidth);
		this.aspectratioHeight = expHeight.intValue();
		LOG.debug("aspectratioHeight: " + aspectratioHeight);
	}

	public String getWmsVersion() {
		return wmsVersion;
	}

	public String getSourceBBOX() {
		return sourceBBOX;
	}

	public double getBboxOffset() {
		return bboxOffset;
	}

	public String getBboxWithOffset() {
		return bboxWithOffset;
	}

	public Double getExpWidth() {
		return expWidth;
	}

	public Double getExpHeight() {
		return expHeight;
	}

	public int getAspectratioWidth() {
		return aspectratioWidth;
	}

	public int getAspectratioHeight() {
		return aspectratioHeight;
	}

	public double getRatio_Width_Height() {
		return ratio_Width_Height;
	}

}