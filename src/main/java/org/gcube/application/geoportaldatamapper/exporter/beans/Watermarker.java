package org.gcube.application.geoportaldatamapper.exporter.beans;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;

import lombok.Data;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "watermarker")
@Data
public class Watermarker {

	@XmlValue
	private String value;

	@XmlAttribute
	private Integer fontSize;

}