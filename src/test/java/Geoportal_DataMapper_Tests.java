
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.gcube.application.geoportal.common.model.document.Project;
import org.gcube.application.geoportalcommon.ConvertToDataValueObjectModel;
import org.gcube.application.geoportalcommon.ProjectDVBuilder;
import org.gcube.application.geoportalcommon.geoportal.GeoportalClientCaller;
import org.gcube.application.geoportalcommon.geoportal.ProjectsCaller;
import org.gcube.application.geoportalcommon.geoportal.UseCaseDescriptorCaller;
import org.gcube.application.geoportalcommon.shared.geoportal.project.ProjectDV;
import org.gcube.application.geoportalcommon.shared.geoportal.view.ProjectView;
import org.gcube.application.geoportaldatamapper.Geoportal_JSON_Mapper;
import org.gcube.application.geoportaldatamapper.shared.ProjectEdit;
import org.gcube.common.authorization.library.provider.SecurityTokenProvider;
import org.gcube.common.scope.api.ScopeProvider;
import org.junit.Before;
import org.junit.Test;

/**
 * The Class Geoportal_DataMapper_Tests.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Dec 5, 2022
 */
public class Geoportal_DataMapper_Tests {

	private static final String GCUBE_CONFIG_PROPERTIES_FILENAME = "gcube_config.properties";
	// APP Working Directory + /src/test/resources must be the location of
	// gcube_config.properties
	private static String gcube_config_path = String.format("%s/%s",
			System.getProperty("user.dir") + "/src/test/resources", GCUBE_CONFIG_PROPERTIES_FILENAME);
	private static String CONTEXT;
	private static String TOKEN;

	private UseCaseDescriptorCaller clientUCD;
	private ProjectsCaller clientProjects;

	//private static String PROFILE_ID = "concessioni-estere";
	//private static String PROFILE_ID = "profiledConcessioni";
	private static String PROFILE_ID = "test_basemodel";
	
	private static String PROJECT_ID = "663cd047c387f9177ca09bcc"; //63d011c4dcac4551b9a6b930 //6442653ac6a0422d9e9042e0 //6399de3ca0a4545420373251

	//IMPORTANT!!!! #NB SET USERNAME = null to test PUBLIC ACCESS
	
	private static String USERNAME = "francesco.mangiacrapa";

	/**
	 * Read context settings.
	 */
	public static void readContextSettings() {

		try (InputStream input = new FileInputStream(gcube_config_path)) {

			Properties prop = new Properties();

			// load a properties file
			prop.load(input);

			CONTEXT = prop.getProperty("CONTEXT");
			TOKEN = prop.getProperty("TOKEN");
			// get the property value and print it out
			System.out.println("CONTEXT: " + CONTEXT);
			System.out.println("TOKEN: " + TOKEN);

		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	/**
	 * Gets the client.
	 *
	 * @return the client
	 */
	@Before
	public void getClient() {
		readContextSettings();
		// assumeTrue(GCubeTest.isTestInfrastructureEnabled());
		ScopeProvider.instance.set(CONTEXT);
		SecurityTokenProvider.instance.set(TOKEN);
		clientUCD = GeoportalClientCaller.useCaseDescriptors();
		clientProjects = GeoportalClientCaller.projects();
	}

	/**
	 * Test read project edit.
	 */
	//@Test
	public void testReadProjectEdit() {

		try {
			ScopeProvider.instance.set(CONTEXT);
			SecurityTokenProvider.instance.set(TOKEN);
			Project theProject = clientProjects.getProjectByID(PROFILE_ID, PROJECT_ID);
			ProjectDVBuilder projectBuilder = ProjectDVBuilder.newBuilder().fullDocumentMap(true);
			projectBuilder.relationships(true);
			ProjectDV theProjectDV = ConvertToDataValueObjectModel.toProjectDV(theProject, projectBuilder);
			ProjectEdit projectEdit = Geoportal_JSON_Mapper.loadProjectEdit(theProjectDV, CONTEXT, USERNAME);
			Geoportal_JSON_Mapper.prettyPrintProjectEdit(projectEdit);

//			ProjectView projectView = Geoportal_JSON_Mapper.loadProjectView(theProjectDV, CONTEXT, USERNAME);
//			Geoportal_JSON_Mapper.prettyPrintProjectView(projectView);
			System.out.println("\n\n testReadProjectEdit terminated!!!");
		} catch (Exception e) {
			System.err.println("Error");
			e.printStackTrace();
		}
	}

	/**
	 * Test read project view.
	 */
	@Test
	public void testReadProjectView() {

		try {
			ScopeProvider.instance.set(CONTEXT);
			SecurityTokenProvider.instance.set(TOKEN);
			Project theProject = clientProjects.getProjectByID(PROFILE_ID, PROJECT_ID);
			ProjectDVBuilder projectBuilder = ProjectDVBuilder.newBuilder().fullDocumentMap(true);
			projectBuilder.relationships(true);

			System.out.println("project json: " + theProject.getTheDocument().toJson());

			ProjectDV theProjectDV = ConvertToDataValueObjectModel.toProjectDV(theProject, projectBuilder);
			ProjectView projectView = Geoportal_JSON_Mapper.loadProjectView(theProjectDV, CONTEXT, USERNAME);
			Geoportal_JSON_Mapper.prettyPrintProjectView(projectView);

//			ProjectView projectView = Geoportal_JSON_Mapper.loadProjectView(theProjectDV, CONTEXT, USERNAME);
//			Geoportal_JSON_Mapper.prettyPrintProjectView(projectView);
			System.out.println("\n\n testReadProjectView terminated!!!");
		} catch (Exception e) {
			System.err.println("Error");
			e.printStackTrace();
		}
	}

}
