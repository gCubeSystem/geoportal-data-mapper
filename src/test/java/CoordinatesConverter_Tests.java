import java.util.ArrayList;

import org.gcube.application.geoportaldatamapper.exporter.gis.WGS84_CoordinatesConverter;
import org.gcube.spatial.data.geoutility.shared.BBOX;
import org.gcube.application.geoportaldatamapper.exporter.gis.BBOXConverter;
import org.gcube.application.geoportaldatamapper.exporter.gis.Recalculate_WGS84_Offset;

public class CoordinatesConverter_Tests {

	private static double EARTH_RADIUS = 6378137.0;

	public static void main(String[] args) {

		// String bbox = "14.350604,40.632238,14.353074,40.634530";
		String source_bbox = "15.208508,41.442976,15.209360,41.443437";
		System.out.println("source bbox: " + source_bbox);

		String wmsVersion = "1.1";
		double bboxOffset = 0.0;
		Recalculate_WGS84_Offset recalculate = new Recalculate_WGS84_Offset(wmsVersion, source_bbox, bboxOffset);

		System.out.println("offset width: " + recalculate.getAspectratioWidth());
		System.out.println("offset heigth: " + recalculate.getAspectratioHeight());
		System.out.println("ratio: " + recalculate.getRatio_Width_Height());
		System.out.println("bbox with offset: " + recalculate.getBboxWithOffset());

		int height = recalculate.getAspectratioHeight();
		int width = recalculate.getAspectratioWidth();

		ArrayList<Double> diff = WGS84_CoordinatesConverter.diffBBOX(source_bbox);
		System.out.println("diff WGS84: " + diff);

		BBOX theBBOX = BBOXConverter.toBBOX(wmsVersion, source_bbox);

		String xMin = new Double(theBBOX.getLowerLeftX()).toString();
		String xMax = new Double(theBBOX.getUpperRightX()).toString();

		Double offsetWidth = BBOXConverter.bboxOffset(xMin, xMax);
		System.out.println("offsetWidth: " + offsetWidth);

		String yMin = new Double(theBBOX.getLowerLeftY()).toString();
		String yMax = new Double(theBBOX.getUpperRightY()).toString();
		Double offsetHeight = BBOXConverter.bboxOffset(yMax, yMin);
		System.out.println("offsetHeight: " + offsetHeight);

//		int expWidth = getExponentForNumber(diff.get(0));
//		int expHeight = getExponentForNumber(diff.get(1));
//		System.out.println("expWidth: " + expWidth);
//		System.out.println("expHeight: " + expHeight);

		if (width < 100 || height < 100) {
			bboxOffset = 0.00001;
		} else if (width < 200 || height < 200) {
			bboxOffset = 0.0001;
		} else if (width < 500 || height < 500) {
			bboxOffset = 0.001;
		} else if (width < 1000 || height < 1000) {
			bboxOffset = 0.01;
		}

		bboxOffset = 0.0001;

		System.out.println("applying offset: " + bboxOffset);

		recalculate = new Recalculate_WGS84_Offset(source_bbox, source_bbox, bboxOffset);

		System.out.println("offset width: " + recalculate.getAspectratioWidth());
		System.out.println("offset heigth: " + recalculate.getAspectratioHeight());
		System.out.println("ratio: " + recalculate.getRatio_Width_Height());
		System.out.println("bbox with offset: " + recalculate.getBboxWithOffset());

	}

	private static int getExponentForNumber(double number) {
		String numberAsString = String.valueOf(number);
		return numberAsString.substring(numberAsString.indexOf('.') + 1).length() * -1;
	}

//	public static void main(String[] args) {
//	
//	// double bboxOffset = 0.003;
//	String theBBOX = "15.208508,41.442976,15.209360,41.443437";
//	double bboxOffset = 0.01; // no offset
//	double lowerXR = theBBOX.getLowerLeftX() - bboxOffset;
//	double lowerYR = theBBOX.getLowerLeftY() - bboxOffset;
//	double upperXR = theBBOX.getUpperRightX() + bboxOffset;
//	double upperYR = theBBOX.getUpperRightY() + bboxOffset;
//
//	String parmBBOX = BBOX.toBBOXString(new BBOX(lowerXR, lowerYR, upperXR, upperYR, ""),
//			bboxFormat);
//
//	// Calculating bounding box dimension
//	String bbox3857 = WGS84_CoordinatesConverter.toEPSG3857_BBOX(parmBBOX);
//	LOG.debug("toEPSG3857_BBOX: " + bbox3857);
//	ArrayList<Double> diff = WGS84_CoordinatesConverter.diffBBox(bbox3857);
//	LOG.debug("BoudingBox width - height: " + diff);
//	double width = diff.get(0);
//	double height = diff.get(1);
//	double ratio = width / height;
//	LOG.trace("bbox width: " + width);
//	LOG.trace("bbox heigth: " + height);
//	LOG.trace("bbox ratio: " + ratio);
//
//	// Calculating width/height ratio
//	if (ratio > 1) {
//		expWidth = expWidth * ratio;
//	} else {
//		expHeight = expHeight * ratio;
//	}
//
//	int aspectratioWidth = expWidth.intValue();
//	int aspectratioHeight = expHeight.intValue();
//
//}

}
