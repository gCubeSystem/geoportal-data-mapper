
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;

import org.gcube.application.geoportalcommon.geoportal.GeoportalClientCaller;
import org.gcube.application.geoportalcommon.geoportal.ProjectsCaller;
import org.gcube.application.geoportalcommon.geoportal.UseCaseDescriptorCaller;
import org.gcube.application.geoportaldatamapper.exporter.Geoportal_PDF_Exporter;
import org.gcube.application.geoportaldatamapper.reader.MapBoxEndpointReader;
import org.gcube.application.geoportaldatamapper.reader.ServiceAccessPoint;
import org.gcube.application.geoportaldatamapper.shared.ExporterProjectSource;
import org.gcube.common.authorization.library.provider.SecurityTokenProvider;
import org.gcube.common.scope.api.ScopeProvider;
import org.gcube.spatial.data.geoutility.shared.BBOX;
import org.junit.Before;
import org.junit.Test;

/**
 * The Class Geoportal_Export_To_PDF_Tests.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Nov 15, 2023
 */
public class Geoportal_Export_To_PDF_Tests {

	private static final String GCUBE_CONFIG_PROPERTIES_FILENAME = "gcube_config.properties";
	// APP Working Directory + /src/test/resources must be the location of
	// gcube_config.properties
	private static String gcube_config_path = String.format("%s/%s",
			System.getProperty("user.dir") + "/src/test/resources", GCUBE_CONFIG_PROPERTIES_FILENAME);
	private static String CONTEXT;
	private static String TOKEN;

	private UseCaseDescriptorCaller clientUCD;
	private ProjectsCaller clientProjects;

	// private static String PROFILE_ID = "concessioni-estere";
	private static String PROFILE_ID = "profiledConcessioni";
	
//	private static String[] PROJECT_IDS = { "6388e4f8a1e60e66b7b584ac", "6388d9b3a1e60e66b7b5843a",
//			"646353c5d7fb4a4633022803", "63f8c481e9d6ac16f394f1e6", "638888fba1e60e66b7b581dd",
//			"650304487814b87373631242", "6388ea17a1e60e66b7b584dd", "64537208d7fb4a4633022039",
//			"638885fea1e60e66b7b581bd" };
	
	//PROD
	//private static String[] PROJECT_IDS = { "6388e4f8a1e60e66b7b584ac" };

	//DEV
	private static String[] PROJECT_IDS = { "661d2c6f8804530afb90b132" };

	// DEV
	// 654e07a75bdd5478cca320c0
	// 655489965bdd5478cca320ea

	// PROD Concessioni
	// 6388e4f8a1e60e66b7b584ac Ficocle-Cervia Vecchia
	// 6388d9b3a1e60e66b7b5843a
	// 646353c5d7fb4a4633022803 Villa Romana del Capo di Sorrento
	// 63f8c481e9d6ac16f394f1e6
	// 638888fba1e60e66b7b581dd
	// 650304487814b87373631242 Scavi a Calvatone-Bedriacum
	// 6388ea17a1e60e66b7b584dd Indagini archeologiche presso la pieve di San Pietro
	// in Bossolo, Barberino Tavarnelle (FI)
	// 64537208d7fb4a4633022039 PArCo di Poggio del Molino - Progetto Archeodig
	// 638885fea1e60e66b7b581bd Scavo della Grotta della Ciota Ciara

	// PRE
	// 63d011c4dcac4551b9a6b930
	// //6442653ac6a0422d9e9042e0
	// //6399de3ca0a4545420373251
	// 645a152fd7fb4a463302240d
	// 63f8c481e9d6ac16f394f1e6

	// PROD concessione-estere
	// 64a2c5c6a827c620191599ff

	// IMPORTANT!!!! #NB SET USERNAME = null to test PUBLIC ACCESS
	private static String USERNAME = null;

	/**
	 * Read context settings.
	 */
	public static void readContextSettings() {

		try (InputStream input = new FileInputStream(gcube_config_path)) {

			Properties prop = new Properties();

			// load a properties file
			prop.load(input);

			CONTEXT = prop.getProperty("CONTEXT");
			TOKEN = prop.getProperty("TOKEN");
			// get the property value and print it out
			System.out.println("CONTEXT: " + CONTEXT);
			System.out.println("TOKEN: " + TOKEN);

		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	/**
	 * Gets the client.
	 *
	 * @return the client
	 */
	//@Before
	public void getClient() {
		readContextSettings();
		// assumeTrue(GCubeTest.isTestInfrastructureEnabled());
		ScopeProvider.instance.set(CONTEXT);
		SecurityTokenProvider.instance.set(TOKEN);
		clientUCD = GeoportalClientCaller.useCaseDescriptors();
		clientProjects = GeoportalClientCaller.projects();
	}

	/**
	 * API map box service endpoint reader.
	 */
	//@Test
	public void apiMapBoxServiceEndpointReader() {
		try {
			List<ServiceAccessPoint> listSAP = MapBoxEndpointReader.getMapBoxEndpoint();

			String result = listSAP.stream().map(sap -> String.valueOf(sap)).collect(Collectors.joining("\n"));

			System.out.println(result);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Test read project view.
	 */
	//@Test
	public void testExportToPDF() {

		ScopeProvider.instance.set(CONTEXT);
		SecurityTokenProvider.instance.set(TOKEN);
		Geoportal_PDF_Exporter pdfExporter = new Geoportal_PDF_Exporter();
		ExporterProjectSource exporterSource = new ExporterProjectSource();
		// #NB SET USERNAME = null to test PUBLIC ACCESS
		USERNAME = "francesco.mangiacrapa";
		exporterSource.setAccountname(USERNAME);
		exporterSource.setProfileID(PROFILE_ID);
		//exporterSource.setProfileTitle("Regime di Concessione");
		exporterSource.setScope(CONTEXT);
		try {

			for (String project_ID : PROJECT_IDS) {
				exporterSource.setProjectID(project_ID);
				pdfExporter.createPDFFile(exporterSource);
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		System.out.println("testExportToPDF terminated");

	}

//	public static void main(String[] args) {
//		GeoServerRESTReader reader = new GeoServerRESTReader("");
//		
//		RESTLayer layer = reader.getLayer("");
//		
//		layer.get
//		
//	}
	/*
	 * public static void main(String[] args) { double imageWidth = 400; double
	 * imageHeight = 400;
	 * 
	 * double expansionFactor = 0.1;
	 * 
	 * String bbox = "8.310868,45.710911,8.310912,45.710940";
	 * 
	 * BBOX theBBOX = new BBOX(bbox, COORDINATE_FORMAT.XY); // double x = imageWidth
	 * * theBBOX.getLowerLeftX(); // double y = imageHeight *
	 * theBBOX.getLowerLeftY() ; // double w = imageWidth *
	 * theBBOX.getUpperRightX(); // double h = imageHeight *
	 * theBBOX.getUpperRightY();
	 * 
	 * Integer[] size = new Integer[2]; size[0] = 400; size[1] = 400; Double[] bboxD
	 * = new Double[4]; bboxD[0] = theBBOX.getLowerLeftX(); bboxD[1] =
	 * theBBOX.getLowerLeftY() ; bboxD[2] = theBBOX.getUpperRightX(); bboxD[3] =
	 * theBBOX.getUpperRightY(); BBOXPixel bboxP =
	 * BBOXConverter.convertToPixel(size, bboxD);
	 * 
	 * System.out.println("bboxP: "+bboxP);
	 * 
	 * double left = Math.max(bboxP.x - bboxP.w * expansionFactor, 0); double top =
	 * Math.max(bboxP.y - bboxP.h * expansionFactor, 0); double width =
	 * Math.min(bboxP.x + bboxP.w * 2 * expansionFactor, imageWidth - left); double
	 * height = Math.min(bboxP.h + bboxP.h * 2 * expansionFactor, imageHeight -
	 * top);
	 * 
	 * BBOXPixel bboxP2 = new BBOXPixel(left, top, width, height);
	 * 
	 * BBOX bboxE = BBOXConverter.convertToCoordinate(bboxP2, size);
	 * System.out.println(bboxE);
	 * 
	 * }
	 */

	static class BoundingBox {
		public double north, south, east, west;

		public BoundingBox(String location, float km) {
			// System.out.println(location + " : "+ km);
			String[] parts = location.replaceAll("\\s", "").split(","); // remove spaces and split on ,
			System.out.println("parts" + " : " + Arrays.asList(parts));

			double lng = Double.parseDouble(parts[0]);
			double lat = Double.parseDouble(parts[1]);

			double adjust = .008983112; // 1km in degrees at equator.
			// adjust = 0.008983152770714983; // 1km in degrees at equator.

			// System.out.println("deg: "+(1.0/40075.017)*360.0);

			north = lat + (km * adjust);
			south = lat - (km * adjust);

			double lngRatio = 1 / Math.cos(Math.toRadians(lat)); // ratio for lng size
			// System.out.println("lngRatio: "+lngRatio);

			east = lng + (km * adjust) * lngRatio;
			west = lng - (km * adjust) * lngRatio;

			System.out.println(new BBOX(west, south, east, north, ""));
		}

	}

//	public static void main(String[] args) {
//	    gdal.AllRegister();
//	    
//	    DataSource dataSource = ogr.Open("https://geoserver-1.cloud-dev.d4science.org/geoserver/profiledconcessioni_gcube_devsec_devvre_654e07a75bdd5478cca320c0/wms?SERVICE=WMS&VERSION=1.1.1&REQUEST=GetMap&FORMAT=image/png&TRANSPARENT=true&STYLES=&LAYERS=profiledconcessioni_gcube_devsec_devvre_654e07a75bdd5478cca320c0:ce19_09&exceptions=application/vnd.ogc.se_inimage&SRS=EPSG:4326&WIDTH=400&HEIGHT=400&BBOX=12.235679626464844,44.179115295410156,12.373008728027344,44.316444396972656");
//	    Layer layer = dataSource.GetLayer(0);
//	    
//	}

	// Mercator projection
	public static ArrayList<Double> convertToPixel(double lng, double lat, double mapWidth, double mapHeight) {
		// get x
		double x = (lng + 180) * (mapWidth / 360);
		// convert from degrees to radians
		double latRad = lat * Math.PI / 180;
		// get y value
		double mercN = Math.log(Math.tan((Math.PI / 4) + (latRad / 2)));
		double y = (mapHeight / 2) - (mapWidth * mercN / (2 * Math.PI));

		ArrayList size = new ArrayList<Double>();
		size.add(x);
		size.add(y);
		return size;
	}

	public static ArrayList<Double> convertPixed(double lng, double lat, double mapWidth, double mapHeight) {
		// get x
		double x = (lng * mapWidth) / 360;
		double y = (lat * mapHeight) / 180;

		ArrayList size = new ArrayList<Double>();
		size.add(x);
		size.add(y);
		return size;
	}

	public static Double[] epsg4326toEpsg3857(Double[] coordinates) {
		double x = (coordinates[0] * 20037508.34) / 180;
		double y = Math.log(Math.tan(((90 + coordinates[1]) * Math.PI) / 360)) / (Math.PI / 180);
		y = (y * 20037508.34) / 180;
		Double[] xy = new Double[2];
		xy[0] = x;
		xy[1] = y;
		return xy;
	}
}
